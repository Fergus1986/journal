package excepciones;

public class InformeNuevoException extends Exception{
	public InformeNuevoException(String titulo, String editor){
		super("El informe con titulo: " + titulo
				+ " y editor: " +editor + " ya existe");
	}
}
