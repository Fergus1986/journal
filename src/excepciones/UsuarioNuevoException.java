package excepciones;

public class UsuarioNuevoException  extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UsuarioNuevoException(String login){
		super("Ya existe un usuario con login: " + login);
	}

}