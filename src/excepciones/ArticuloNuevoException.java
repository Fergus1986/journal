package excepciones;

public class ArticuloNuevoException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ArticuloNuevoException(String titulo, String autor){
		super("El articulo con titulo: " + titulo
				+ " y autor: " + autor + " ya existe");
	}

}
