package excepciones;

public class UsuarioErroneo extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UsuarioErroneo(String login, String pass){
		super("El usuario con login:" + login
				+ " y contraseña " + pass + " no existe");
	}

}
