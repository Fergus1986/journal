package excepciones;

public class NoHayConexionException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoHayConexionException(){
		super("No hay conexiones disponibles a la BD");
	}
	
	
}
