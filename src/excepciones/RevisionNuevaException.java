package excepciones;

public class RevisionNuevaException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public RevisionNuevaException(String titulo, String revisor){
		super("La revision del articulo con titulo: " + titulo
				+ " y revisor: " + revisor + " ya existe");
	}

}
