package excepciones;

public class ArticuloErroneoException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public ArticuloErroneoException(String titulo, String autor){
		super("El articulo con titulo: " + titulo
				+ " y autor: " + autor + " no existe");
	}
}
