package excepciones;

public class RevisionErroneaException extends Exception{
	private static final long serialVersionUID = 1L;
	
	public RevisionErroneaException(String titulo, String revisor){
		super("La revision del articulo con titulo: " + titulo
				+ " y revisor: " + revisor + " no existe");
	}
}
