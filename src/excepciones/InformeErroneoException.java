package excepciones;

public class InformeErroneoException extends Exception{

	private static final long serialVersionUID = 1L;

	public InformeErroneoException(String titulo, String editor){
		super("El infome con titulo: " + titulo + " y editor: " + editor + "no existe");
	}
}
