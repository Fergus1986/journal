package excepciones;

public class UsuarioErroneoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UsuarioErroneoException(String login, String pass){
		super("El usuario con login:" + login
				+ " y contraseña " + pass + " no existe");
	}

}
