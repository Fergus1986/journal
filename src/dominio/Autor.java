package dominio;

import java.util.Vector;

import excepciones.ArticuloErroneoException;
import excepciones.NoHayConexionException;

import persistencia.ArticuloDAO;

public class Autor extends Usuario{
	
	private Vector<Articulo> articulos;
	
	public Autor(Usuario usuario){
		
		this.setLogin(usuario.getLogin());
		this.setNombre(usuario.getNombre());
		this.setApellido(usuario.getApellido());
	}
	
	public void consultarArticulos() throws Exception{
		this.setArticulos(ArticuloDAO.getArticulos(this.getLogin()));	
	}
	
	public Articulo consultarArticulo(String titulo) throws Exception{
		Articulo temporal = ArticuloDAO.getArticulo(titulo, this.getLogin());
		return temporal;
	}

	public Vector<Articulo> getArticulos() {
		return articulos;
	}

	public void setArticulos(Vector<Articulo> articulos) {
		this.articulos = articulos;
	}

}
