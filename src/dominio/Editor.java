package dominio;
import java.util.Vector;
import persistencia.InformeDAO;

public class Editor extends Usuario{
		
		private Vector<Informe> informes;
		
		public Editor(Usuario usuario){
			
			this.setLogin(usuario.getLogin());
			this.setNombre(usuario.getNombre());
			this.setApellido(usuario.getApellido());
		}
		
		public void consultarInformes() throws Exception{
			
			this.setInformes(InformeDAO.getInformes(this.getLogin()));	
//			System.out.println("Informes recuperdados en la clase Editor "+InformeDAO.getInformes(this.getLogin()));
		}
		
		public Informe consultarInforme(String titulo) throws Exception{
			Informe temporal = InformeDAO.getInforme(titulo, this.getLogin());
			return temporal;
		}

		public Vector<Informe> getInformes() {
			return informes;
		}

		public void setInformes(Vector<Informe> informes) {
			this.informes = informes;
		}

	}


