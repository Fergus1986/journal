package dominio;

import persistencia.UsuarioDAO;

public class Usuario {
	private String login, pass, nombre, apellido, rol,sin_cifrar;
	private int nrevisiones;
	private int id;
	
	public Usuario(){
		
	}
	
	public Usuario(String login, boolean autor) throws Exception{
		if(autor) this.sin_cifrar = UsuarioDAO.insertarNuevoUsuario(login, "autor");
		else this.sin_cifrar = UsuarioDAO.insertarNuevoUsuario(login, "revisor");
		
		this.login = login;
		this.pass = UsuarioDAO.recuperarPass(login);
		
	}
	
	public Usuario(String login, String pass) throws Exception{
		Usuario temporal = UsuarioDAO.getUsuario(login, pass);
		
		this.setLogin(temporal.getLogin());
		this.setPass(temporal.getPass());
		this.setNombre(temporal.getNombre());
		this.setApellido(temporal.getApellido());
		this.setRol(temporal.getRol());
		
	}
	
	public void modificar() throws Exception {
		if(this.rol.equals("revisor")) 
			UsuarioDAO.actualizarUsuario(login, pass, rol, nombre, apellido, UsuarioDAO.revisionesRevisor(this.login));
		else
			UsuarioDAO.actualizarUsuario(login, pass, rol, nombre, apellido, 0);
	}
	
	public void getEditor() throws Exception {
		UsuarioDAO.getEditor(this);
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getSin_cifrar() {
		return sin_cifrar;
	}

	public void setSin_cifrar(String sin_cifrar) {
		this.sin_cifrar = sin_cifrar;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNrevisiones() {
		return nrevisiones;
	}

	public void setNrevisiones(int nrevisiones) {
		this.nrevisiones = nrevisiones;
	}
	
}
