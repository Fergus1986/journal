package dominio;

import java.util.Vector;

import persistencia.RevisionDAO;

public class Revisor extends Usuario {

	protected Vector<Revision> revisionesAsignadas;
	protected Vector<Revision> revisionesAceptadas;
	protected Vector<Revision> revisionesTerminadas;
	
	Revision temporal;
	protected int nrevisiones;
	
	public int getNrevisiones() {
		return nrevisiones;
	}

	public void setNrevisiones(int nrevisiones) {
		this.nrevisiones = nrevisiones;
	}

	public Revisor(Usuario usuario){
		
		this.setLogin(usuario.getLogin());
		this.setNombre(usuario.getNombre());
		this.setApellido(usuario.getApellido());
	}
	
	public void consultarRevision(String tituloArticulo) throws Exception{
		temporal = RevisionDAO.getRevision(tituloArticulo, this.getLogin());
	}
	
	public Revision getRevision(){
		return this.temporal;
	}
	
	public void setRevision(Revision temporal){
		this.temporal = temporal;
	}
	
	public void consultarRevisionesAsignadas() throws Exception{
//		System.out.println("Revisor - RevisionesAsignadas");
		this.setRevisionesAsignadas(RevisionDAO.getRevisiones(this.getLogin(), "asignada"));
	}
	
	
	public void consultarRevisionesAceptadas() throws Exception{
//		System.out.println("Revisor - RevisionesAceptadas");
		this.setRevisionesAceptadas(RevisionDAO.getRevisiones(this.getLogin(), "aceptada"));
	}
	
	public void consultarRevisionesTerminadas() throws Exception{
//		System.out.println("Revisor - RevisionesTerminadas");
		this.setRevisionesTerminadas(RevisionDAO.getRevisiones(this.getLogin(), "terminada"));
	}

	public Vector<Revision> getRevisionesAsignadas() {
		return revisionesAsignadas;
	}

	public void setRevisionesAsignadas(Vector<Revision> revisionesAsignadas) {
		this.revisionesAsignadas = revisionesAsignadas;
	}

	public Vector<Revision> getRevisionesAceptadas() {
		return revisionesAceptadas;
	}

	public void setRevisionesAceptadas(Vector<Revision> revisionesAceptadas) {
		this.revisionesAceptadas = revisionesAceptadas;
	}

	public Vector<Revision> getRevisionesTerminadas() {
		return revisionesTerminadas;
	}

	public void setRevisionesTerminadas(Vector<Revision> revisionesTerminadas) {
		this.revisionesTerminadas = revisionesTerminadas;
	}
	

}
