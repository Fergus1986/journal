package dominio;

import persistencia.ArticuloDAO;

public class Articulo {
	
	private String titulo;
	private String autor;
	private String temas;
	private byte[] archivo;
	private String estado;
	private int nrevisiones;
	private String informe;
	
	private int id;
	
	public Articulo(){
		
	}
	
	public Articulo(String titulo, String autor) throws Exception{
		Articulo temporal = ArticuloDAO.getArticulo(titulo, autor);
		this.setTitulo(temporal.getTitulo());
		this.setAutor(temporal.getAutor());
		this.setTemas(temporal.getTemas());
		this.setArchivo(temporal.getArchivo());
		this.setEstado(temporal.getEstado());
		this.setNrevisiones(temporal.getNrevisiones());
		this.setInforme(temporal.getInforme());
	}
	
	public Articulo(String titulo,
			String autor,
			String temas,
			byte[] archivo,
			String estado,
			int nrevisiones) throws Exception{
		
		ArticuloDAO.nuevoArticulo(titulo, autor, temas, archivo, estado, nrevisiones);
		
	}
		
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTemas() {
		return temas;
	}
	public void setTemas(String temas) {
		this.temas = temas;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNrevisiones() {
		return nrevisiones;
	}

	public void setNrevisiones(int nrevisiones) {
		this.nrevisiones = nrevisiones;
	}

	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

}
