package dominio;

import persistencia.ArticuloDAO;
import persistencia.RevisionDAO;
import persistencia.UsuarioDAO;

public class Revision {
	
	private Articulo articulo;
	private String revisor;
	private String estado;
	private int baremo;	
	private byte[] archivo;
	
	public static void rechazar(String tituloArticulo, String revisor) {
		RevisionDAO.rechazarRevision(tituloArticulo, revisor);
	}
	
	public Revision(){
		
	}
	
	public Revision(String tituloArticulo, String revisor, String estado, int baremo, byte[] archivo) throws Exception{
		RevisionDAO.nuevaRevision(tituloArticulo, revisor,estado,baremo,archivo);
	}
	
	public Revision(String tituloArticulo, String revisor) throws Exception{
		RevisionDAO.getRevision(tituloArticulo, revisor);
	}
	public Revision(String tituloArticulo, String revisor, String estado) throws Exception{
		RevisionDAO.nuevaRevision(tituloArticulo, revisor, estado);
		if(estado.equals("aceptada")){
			ArticuloDAO.modificarArticulo(tituloArticulo);
			UsuarioDAO.modificarRevisor(revisor);
			}
	}
	

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public String getTituloArticulo() {
		return this.getArticulo().getTitulo();
	}
	
	public String getTemasArticulo() {
		return this.getArticulo().getTemas();
	}

	public String getRevisor() {
		return revisor;
	}

	public void setRevisor(String revisor) {
		this.revisor = revisor;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public int getBaremo() {
		return baremo;
	}

	public void setBaremo(int baremo) {
		this.baremo = baremo;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}
}
