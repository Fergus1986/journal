package dominio;

import persistencia.InformeDAO;

public class Informe {
	private String titulo;
	//Lo pongo así para que no de errores
	private String editor;
	private String articulo;
	private byte[] informe;
	private String autor;
	private byte[] archivo;

	public Informe(){	
	}
	
	public Informe(String titulo, String editor) throws Exception{
		Informe temporal = InformeDAO.getInforme(titulo, editor);
		this.setTitulo(temporal.getTitulo());
		this.setEditor(temporal.getEditor());
		this.setInforme(temporal.getInforme());
		this.setArticulo(temporal.getArticulo());
		this.setAutor(temporal.getAutor());
		this.setArchivo(temporal.getArchivo());
	}
	
	public Informe(String titulo, String editor,byte[] informe, String articulo) throws Exception{
		InformeDAO.nuevoInforme(titulo, editor, informe,articulo);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getArticulo() {
		return articulo;
	}

	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}

	public byte[] getInforme() {
		return informe;
	}

	public void setInforme(byte[] informe) {
		this.informe = informe;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public byte[] getArchivo() {
		return archivo;
	}

	public void setArchivo(byte[] archivo) {
		this.archivo = archivo;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

}
