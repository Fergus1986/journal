package acciones.autor;
import javax.servlet.http.HttpServletRequest;  
  
import org.apache.commons.io.FileUtils;  
import org.apache.struts2.interceptor.SessionAware;

import util.Archivo;
import util.Email;

import java.io.*;
import java.util.Map;


import com.mysql.jdbc.Blob;
import com.opensymphony.xwork2.ActionInvocation;

import dominio.Articulo;
import dominio.Usuario;
  
public class SubirArticulo extends GestionarArticulo implements SessionAware {  

	private Map<String, Object> sesion;
	private static final long serialVersionUID = 1L;
	private File upload;  
    private String uploadContentType;  
    private String uploadFileName;  
    private HttpServletRequest servletRequest;
      
    public HttpServletRequest getServletRequest() {  
        return servletRequest;  
    }  
  
    public void setServletRequest(HttpServletRequest servletRequest) {  
        this.servletRequest = servletRequest;  
    }  
  
    public File getUpload(){  
        return upload;  
    }  
  
    public String getUploadContentType() {  
        return uploadContentType;  
    }  
  
    public String getUploadFileName() {  
        return uploadFileName;  
    }  
  
    public void setUpload(File upload) {  
        this.upload = upload;  
    }  
  
    public void setUploadContentType(String uploadContentType) {  
        this.uploadContentType = uploadContentType;  
    }  
  
    public void setUploadFileName(String uploadFileName) {  
        this.uploadFileName = uploadFileName;  
    }  
  
    public String execute() {  
        try {        	 
             File theFile = new File(this.getUploadFileName());  
             FileUtils.copyFile(upload,theFile);
             
             Archivo archivo = new Archivo(theFile);
             
//             InputStream io = (InputStream) new FileInputStream(theFile);
//             
        	
        	 Usuario usuario = (Usuario) sesion.get("usuario");
        	 
        	 this.autor = usuario.getLogin();
//        	 this.articulo.setArchivo(io);

//             System.out.println("Titulo: " + this.titulo);
//             System.out.println("Autor: " + this.autor);
//             System.out.println("Temas: " + this.temas);
//             System.out.println("Size: " + theFile.length());
//             
             
             this.articulo = new Articulo(this.titulo, this.autor, this.temas, archivo.getBytes(), "revision", 0);           
             Usuario editor = new Usuario();
 			 editor.getEditor();
 			 String concepto="Artículo nuevo subido";
 			 String mensaje="El autor "+this.autor+ " ha subido un artículo nuevo al sistema con título "+ this.titulo+" y temas "+this.temas+".";
             Email email=new Email(editor.getLogin(), concepto, mensaje);
             System.out.println("Destinatario "+editor.getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);
                    
             
        } catch (Exception ex) {  
            ex.printStackTrace();  
        }  
        return SUCCESS;  
    }  

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}
}