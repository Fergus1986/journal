package acciones.autor;

import java.util.Vector;

import com.opensymphony.xwork2.ActionSupport;

import dominio.Articulo;

public class GestionarArticulo extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	protected String titulo;
	protected String autor;
	protected String temas;
	protected Articulo articulo;
	
	protected Vector<Articulo> articulos;

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTemas() {
		return temas;
	}
	public void setTemas(String temas) {
		this.temas = temas;
	}
	
	public Vector<Articulo> getArticulos() {
		return articulos;
	}
	public void setArticulos(Vector<Articulo> articulos) {
		this.articulos = articulos;
	}
	
	public void anadirArticulo(Articulo articulo){
		this.articulos.add(articulo);
		
	}
	public Articulo getArticulo() {
		return articulo;
	}
	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

}
