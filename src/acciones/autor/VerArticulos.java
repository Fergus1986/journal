package acciones.autor;

import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;


import dominio.Autor;
import dominio.Usuario;

public class VerArticulos extends GestionarArticulo implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	public String execute() {
					
		try{
			
			Usuario usuario = (Usuario) sesion.get("usuario");
//			System.out.println("Autor: " + usuario.getLogin());
			
			Autor autor = new Autor(usuario);
			
			autor.consultarArticulos();
			
//			System.out.println("Articulos leidos");
			
			this.articulos = autor.getArticulos();

			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
