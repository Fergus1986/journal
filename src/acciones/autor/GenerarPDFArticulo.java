package acciones.autor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Autor;
import dominio.Usuario;

public class GenerarPDFArticulo extends GestionarArticulo implements SessionAware {
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private InputStream inputStream;
	private String descarga;
	private String accion;
	
	public String execute(){
		
		try{
			
//			System.out.println("GenerarPDFArticulo");
			
			Usuario usuario = (Usuario) sesion.get("usuario");
			
			Autor autor = new Autor (usuario);
//			System.out.println("Generar PDF: " + descarga);
			inputStream = new ByteArrayInputStream(autor.consultarArticulo(descarga).getArchivo());
			
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescarga() {
		return descarga;
	}

	public void setDescarga(String descarga) {
		this.descarga = descarga;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}
	
}
