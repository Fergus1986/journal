package acciones.autor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import dominio.Editor;
import dominio.Usuario;

public class GenerarPDFArticuloInforme extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	private InputStream inputStream;
	private String descarga;
	
	public String execute(){
		
		try{
			
			Usuario temp = new Usuario();
			temp.getEditor();
			Editor editor = new Editor(temp);
			
			inputStream = new ByteArrayInputStream(editor.consultarInforme(descarga).getInforme());
			
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Informe erróneo!");
			return ERROR;
		}
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescarga() {
		return descarga;
	}

	public void setDescarga(String descarga) {
		this.descarga = descarga;
	}
}
