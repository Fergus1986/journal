package acciones.revisor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Revisor;
import dominio.Usuario;

public class GenerarPDFRevision extends GestionarRevision implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private InputStream inputStream;
	private String descarga;
	
	public String execute(){
		
		try{
			
//			System.out.println("GenerarPDFRevision");
			
			Usuario usuario = (Usuario) sesion.get("usuario");
			
			Revisor revisor  = new Revisor (usuario);
//			System.out.println("Generar PDF: " + descarga);
					
			revisor.consultarRevision(descarga);
			
//			System.out.println("Revision: " + revisor.getRevision().getArchivo().toString());
			
			inputStream = new ByteArrayInputStream(revisor.getRevision().getArchivo());	
			
			
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Revision erróneo!");
			return ERROR;
		}
	}
	
	
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescarga() {
		return descarga;
	}

	public void setDescarga(String descarga) {
		this.descarga = descarga;
	}
}
