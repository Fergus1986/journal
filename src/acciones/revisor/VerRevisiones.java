package acciones.revisor;

import java.util.Map;
import java.util.Vector;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Revision;
import dominio.Revisor;
import dominio.Usuario;

public class VerRevisiones extends GestionarRevision implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	public String execute() {
					
		try{
//			System.out.println("Ver Revisiones");
			Usuario usuario = (Usuario) sesion.get("usuario");
//			System.out.println("Revisor: " + usuario.getLogin());
			
			Revisor revisor = new Revisor(usuario);
			
			revisor.consultarRevisionesAsignadas();
			revisor.consultarRevisionesAceptadas();
			revisor.consultarRevisionesTerminadas();
			
			this.setRevisionesAsignadas(revisor.getRevisionesAsignadas());
//			System.out.println("Revisiones Asignadas: " + this.getRevisionesAsignadas().size());
			
			this.setRevisionesAceptadas(revisor.getRevisionesAceptadas());
//			System.out.println("Revisiones Aceptadas: " + this.getRevisionesAceptadas().size());

			this.setRevisionesTerminadas(revisor.getRevisionesTerminadas());
//			System.out.println("Revisiones Terminadas: " + this.getRevisionesTerminadas().size());

			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
