package acciones.revisor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Revisor;
import dominio.Usuario;

public class GenerarPDFArticuloRevision extends GestionarRevision implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private InputStream inputStream;
	private String descarga;
	
	public String execute(){
		
		try{
			
//			System.out.println("GenerarPDFArticulo");
			
			Usuario usuario = (Usuario) sesion.get("usuario");
			
			Revisor revisor  = new Revisor (usuario);
//			System.out.println("Generar PDF: " + descarga);
			
//			System.out.println("Consultar Articulo: " + descarga);
			revisor.consultarRevision(descarga);
			inputStream = new ByteArrayInputStream(revisor.getRevision().getArticulo().getArchivo());
			
			
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}
	
	
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescarga() {
		return descarga;
	}

	public void setDescarga(String descarga) {
		this.descarga = descarga;
	}

}
