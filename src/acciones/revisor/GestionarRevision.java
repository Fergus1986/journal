package acciones.revisor;

import java.util.Vector;

import com.opensymphony.xwork2.ActionSupport;
import dominio.Revision;

public class GestionarRevision extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	protected String tituloArticulo;
	protected String revisor;
	protected Revision revision;
	protected String estado;
	protected int baremo;
	protected String titulo;
	protected int nrevisiones;
	
	public int getNrevisiones() {
		return nrevisiones;
	}

	public void setNrevisiones(int nrevisiones) {
		this.nrevisiones = nrevisiones;
	}

	protected Vector<Revision> revisionesAsignadas;
	protected Vector<Revision> revisionesAceptadas;
	protected Vector<Revision> revisionesTerminadas;
	
	public String getTitulo(){
		return titulo;
	}
	
	public void setTitulo(String titulo){
		this.titulo = titulo;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Revision getRevision() {
		return revision;
	}
	
	public void setRevision(Revision revision) {
		this.revision = revision;
	}
	
	public String getTituloArticulo() {
		return tituloArticulo;
	}
	
	public void setTituloArticulo(String tituloArticulo) {
		this.tituloArticulo = tituloArticulo;
	}
	
	public int getBaremo() {
		return baremo;
	}
	
	public void setBaremo(int baremo) {
		this.baremo = baremo;
	}
	
	public String getRevisor() {
		return revisor;
	}
	
	public void setRevisor(String revisor) {
		this.revisor = revisor;
	}
	
	public Vector<Revision> getRevisionesAsignadas() {
		return revisionesAsignadas;
	}
	
	public void setRevisionesAsignadas(Vector<Revision> revisionesAsignadas) {
		this.revisionesAsignadas = revisionesAsignadas;
	}
	
	public Vector<Revision> getRevisionesAceptadas() {
		return revisionesAceptadas;
	}
	
	public void setRevisionesAceptadas(Vector<Revision> revisionesAceptadas) {
		this.revisionesAceptadas = revisionesAceptadas;
	}
	
	public Vector<Revision> getRevisionesTerminadas() {
		return revisionesTerminadas;
	}
	
	public void setRevisionesTerminadas(Vector<Revision> revisionesTerminadas) {
		this.revisionesTerminadas = revisionesTerminadas;
	}
}
