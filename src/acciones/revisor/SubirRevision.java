package acciones.revisor;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;  
  
import org.apache.commons.io.FileUtils;  
import org.apache.struts2.interceptor.SessionAware;

import persistencia.UsuarioDAO;

import util.Archivo;
import util.Email;

import dominio.Revision;
import dominio.Revisor;
import dominio.Usuario;

import java.io.*;

  
public class SubirRevision extends GestionarRevision implements SessionAware {  

	private Map<String, Object> sesion;
	private static final long serialVersionUID = 1L;
	private File upload;  
    private String uploadContentType;  
    private String uploadFileName;  
    private HttpServletRequest servletRequest;
      
    public HttpServletRequest getServletRequest() {  
        return servletRequest;  
    }  
  
    public void setServletRequest(HttpServletRequest servletRequest) {  
        this.servletRequest = servletRequest;  
    }  
  
    public File getUpload(){  
        return upload;  
    }  
  
    public String getUploadContentType() {  
        return uploadContentType;  
    }  
  
    public String getUploadFileName() {  
        return uploadFileName;  
    }  
  
    public void setUpload(File upload) {  
        this.upload = upload;  
    }  
  
    public void setUploadContentType(String uploadContentType) {  
        this.uploadContentType = uploadContentType;  
    }  
  
    public void setUploadFileName(String uploadFileName) {  
        this.uploadFileName = uploadFileName;  
    }  
  
    public String execute() {  
		try {
			File theFile = new File(this.getUploadFileName());
			FileUtils.copyFile(upload, theFile);

			Usuario usuario = (Usuario) sesion.get("usuario");
			Archivo archivo = new Archivo(theFile);

			this.revisor = usuario.getLogin();
//
//			System.out.println("Titulo: " + this.titulo);
//			System.out.println("Revisor: " + this.revisor);
//			System.out.println("Size: " + theFile.length());

			this.revision = new Revision(this.titulo, usuario.getLogin(),
					"terminada", this.baremo, archivo.getBytes());

			Usuario editor = new Usuario();
			editor.getEditor();
			Revisor revisor = new Revisor(usuario);
//			Revision.rechazar(tituloArticulo, revisor.getLogin());
			UsuarioDAO.revisionTerminada(this.revisor);

			String concepto = "Revisión finalizada";
			String mensaje = "El revisor " + this.revisor
					+ " ha finalizado la revisión del artículo con título "
					+ this.titulo + ".\n";

			Email email = new Email(editor.getLogin(), concepto, mensaje);

			System.out.println("Destinatario "+editor.getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);

		} catch (Exception ex) {
            ex.printStackTrace();  
        }  
        return SUCCESS;  
    }  

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}
	public void validate(){
		if(baremo<1 || baremo>4){
			addFieldError("baremo", "El baremo debe estar entre 1 y 4");
		}
	}
}