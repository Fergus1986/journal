package acciones.revisor;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import persistencia.UsuarioDAO;

import util.Email;

import dominio.Revision;
import dominio.Revisor;
import dominio.Usuario;

public class AceptarRevision extends GestionarRevision implements SessionAware {

	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	public String execute() throws Exception {
		Email email;
		try{
			Usuario usuario = (Usuario) sesion.get("usuario");
			Usuario editor = new Usuario();
			editor.getEditor();
			Revisor revisor = new Revisor(usuario);
			
			String concepto, mensaje;
			if(UsuarioDAO.revisionesRevisor(revisor.getLogin())<10){
				Revision temporal = new Revision(this.tituloArticulo, revisor.getLogin(), "aceptada");
//				System.out.println("Las revisiones que tiene el revisor en el if son: "+revisor.getNrevisiones());
				concepto ="Revisión aceptada";
				 mensaje="El revisor "+ revisor.getLogin() + " ha aceptado revisar el artículo con título "
				+ this.tituloArticulo + ".\n";			
//				System.out.println("Mensaje "+mensaje);
			}else{
//				System.out.println("Las revisiones que tiene el revisor en el else son: "+revisor.getNrevisiones());
				Revision.rechazar(this.tituloArticulo, revisor.getLogin());
			    concepto="Revisión rechazada automáticamente";
				mensaje="El revisor "+revisor.getLogin()+ " tiene demasiadas revisiones aceptadas";
				email=new Email(revisor.getLogin(), concepto,"¡Tienes demasiadas revisiones!");
				System.out.println("Destinatario "+revisor.getLogin()+"\nConcepto "+concepto+"\nMensaje "+"¡Tienes demasiadas revisiones!");
//				System.out.println("Mensaje "+mensaje);
			}
			
			email = new Email(editor.getLogin(), concepto, mensaje);
			System.out.println("Destinatario "+editor.getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);
//			System.out.println("Email creado");
			
			return SUCCESS;
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
