package acciones.revisor;

import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import persistencia.RevisionDAO;
import com.opensymphony.xwork2.ActionSupport;

import dominio.Usuario;

public class ConsultarRevisiones extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	private ArrayList<String>  revisiones;
	private ArrayList<Integer> baremos;
	
	public String execute() {
		
		try {
			Usuario usuario = (Usuario) sesion.get("usuario");
			this.setRevisiones(new ArrayList<String> ());
			this.setBaremos(new ArrayList<Integer> ());
			
			this.setRevisiones(RevisionDAO.getAceptadas(usuario.getLogin()));
			
			this.baremos.add(new Integer(1));
			this.baremos.add(new Integer(2));
			this.baremos.add(new Integer(3));
			this.baremos.add(new Integer(4));
			
			this.getSesion().put("revisiones", this.getRevisiones());
			this.getSesion().put("baremos", this.getBaremos());
			return SUCCESS;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
    }
	
	public Map<String, Object> getSesion() {
		return sesion;
	}

	public void setSesion(Map<String, Object> sesion) {
		this.sesion = sesion;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.setSesion(arg0);
		
	}

	public ArrayList<String> getRevisiones() {
		return revisiones;
	}

	public void setRevisiones(ArrayList<String> revisiones) {
		this.revisiones = revisiones;
	}

	public ArrayList<Integer> getBaremos() {
		return baremos;
	}

	public void setBaremos(ArrayList<Integer> baremos) {
		this.baremos = baremos;
	}

}
