package acciones.revisor;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import util.Email;

import dominio.Revision;
import dominio.Revisor;
import dominio.Usuario;

public class RechazarRevision extends GestionarRevision implements SessionAware {

	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	public String execute() {
		
		try{
			Usuario usuario = (Usuario) sesion.get("usuario");
			Usuario editor = new Usuario();
			editor.getEditor();
			Revisor revisor = new Revisor(usuario);
			Revision.rechazar(tituloArticulo, revisor.getLogin());
			
			String concepto ="Revisión rechazada";
			String mensaje="El revisor "+ revisor.getLogin() + " ha rechazado revisar el artículo con título "
			+ this.tituloArticulo + ".\n";					
			System.out.println("Mensaje "+mensaje);
			Email email = new Email(editor.getLogin(), concepto, mensaje);
			
			System.out.println("Destinatario "+editor.getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);
			
			return SUCCESS;
			
		}catch(Exception e){
			addActionError("¡Nombre de Articulo erróneo!");
			return ERROR;
		}
	}
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
