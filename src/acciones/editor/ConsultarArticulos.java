package acciones.editor;

import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import persistencia.ArticuloDAO;

import com.opensymphony.xwork2.ActionSupport;

public class ConsultarArticulos extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> sesion;
	private ArrayList<String> articulos;
	
public String execute() {
		
		try {
			
			this.articulos = new ArrayList<String> ();
			
			this.setArticulos(ArticuloDAO.getArticulosInforme());
			
			this.getSesion().put("articulos", this.getArticulos());
			return SUCCESS;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
    }
	
	public ArrayList<String> getArticulos() {
		return articulos;
	}

	public void setArticulos(ArrayList<String> articulos) {
		this.articulos = articulos;
	}
	
	public Map<String, Object> getSesion() {
		return sesion;
	}

	public void setSesion(Map<String, Object> sesion) {
		this.sesion = sesion;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.setSesion(arg0);
		
	}
}
