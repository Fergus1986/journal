	package acciones.editor;

	import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

	import org.apache.struts2.interceptor.SessionAware;

	import dominio.Editor;
import dominio.Informe;
import dominio.Usuario;

public class GenerarPDFInformeArticulo extends GestionarInforme implements SessionAware{
		private static final long serialVersionUID = 1L;
		private Map<String, Object> sesion;
		private InputStream inputStream;
		private String descarga1;
		//private String descarga_articulo;
		public String execute(){
			
			try{
				
//				System.out.println("Prueba");
				
				Usuario usuario = (Usuario) sesion.get("usuario");			
				Editor editor = new Editor(usuario);
				
//				System.out.println("Usuario: " + editor.getLogin());
//		        System.out.println(descarga+","+descarga_articulo);
				Informe temporal = editor.consultarInforme(descarga1);
				inputStream = new ByteArrayInputStream(temporal.getArchivo());
//							
//				System.out.println("Final");
				
				return SUCCESS;			
				
			}catch(Exception e){
				addActionError("¡Nombre de Informe erróneo!");
				return ERROR;
			}
		}
		
		@Override
		public void setSession(Map<String, Object> arg0) {
			this.sesion=arg0;
			
		}

		public InputStream getInputStream() {
			return inputStream;
		}

		public void setInputStream(InputStream inputStream) {
			this.inputStream = inputStream;
		}

		public String getDescarga1() {
			return descarga1;
		}

		public void setDescarga1(String descarga) {
			this.descarga1 = descarga;
		}
		
	}
