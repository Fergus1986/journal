package acciones.editor;


import util.Email;

import com.opensymphony.xwork2.ActionSupport;

import dominio.Revision;


public class AsignarRevisor extends ActionSupport {
	private static final long serialVersionUID = 1L;
	private String articulo;
	private String revisor;
	
		
		public String execute() {
			
			try {
				
//				System.out.println("Articulo: " + this.articulo);
//				System.out.println("Revisor: " + this.revisor);
				Revision temporal=new Revision(this.articulo, this.revisor,"asignada");
				
				String concepto ="Revisión asignada";
				String mensaje="Se le ha asignado como revisor del artículo "
				+articulo + " en Journal!!\n";
						
				
				Email email = new Email(this.revisor, concepto, mensaje);
				
				System.out.println("Destinatario "+this.revisor+"\nConcepto "+concepto+"\nMensaje "+mensaje);
							
				return SUCCESS;
				
			} catch (Exception e) {
				System.out.println("Error: Asignar Revisor");
				e.printStackTrace();
				return ERROR;
			}
	    }


		public String getArticulo() {
			return articulo;
		}


		public void setArticulo(String articulo) {
			this.articulo = articulo;
		}


		public String getRevisor() {
			return revisor;
		}


		public void setRevisor(String revisor) {
			this.revisor = revisor;
		}
		
	}

	

