package acciones.editor;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Editor;
import dominio.Usuario;

public class GenerarPDFInforme extends GestionarInforme implements SessionAware {
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private InputStream inputStream;
	private String descarga;
	public String execute(){
		
		try{
			
			Usuario usuario = (Usuario) sesion.get("usuario");			
			Editor editor = new Editor(usuario);
			
			inputStream = new ByteArrayInputStream(editor.consultarInforme(descarga).getInforme());
			
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Informe erróneo!");
			return ERROR;
		}
	}
	
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescarga() {
		return descarga;
	}

	public void setDescarga(String descarga) {
		this.descarga = descarga;
	}
	
}

