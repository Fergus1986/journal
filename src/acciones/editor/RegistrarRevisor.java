package acciones.editor;

import acciones.usuario.GestionarUsuario;
import util.Email;
import dominio.Usuario;


public class RegistrarRevisor extends GestionarUsuario{
	
	private static final long serialVersionUID = 1L;
	
	public String execute() {
		
		try {
			this.setUsuario(new Usuario(this.getLogin(), false));
			String concepto ="Registro de usuario en Journal";
			String mensaje="Se le ha registrado como revisor en Journal!!\n" +
					"\nUsuario: " + this.getUsuario().getLogin() + 
					"\nPassword: " + this.getUsuario().getSin_cifrar() + "\n";
			Email email = new Email(this.getUsuario().getLogin(),concepto, mensaje);
			System.out.println("Destinatario "+this.getUsuario().getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);
//			System.out.println("Email creado");
						
			return SUCCESS;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
    }
}
