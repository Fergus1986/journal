package acciones.editor;
import javax.servlet.http.HttpServletRequest;  
import org.apache.commons.io.FileUtils;  
import org.apache.struts2.interceptor.SessionAware;
import util.Archivo;
import util.Email;

import java.io.*;
import java.util.Map;
import dominio.Informe;
import dominio.Usuario;
import dominio.Articulo;
  
public class SubirInforme extends GestionarInforme implements SessionAware {  

	private Map<String, Object> sesion;
	private static final long serialVersionUID = 1L;
	private File upload;  
    private String uploadContentType;  
    private String uploadFileName;  
    private HttpServletRequest servletRequest;
      
    public HttpServletRequest getServletRequest() {  
        return servletRequest;  
    }  
  
    public void setServletRequest(HttpServletRequest servletRequest) {  
        this.servletRequest = servletRequest;  
    }  
  
    public File getUpload(){  
        return upload;  
    }  
  
    public String getUploadContentType() {  
        return uploadContentType;  
    }  
  
    public String getUploadFileName() {  
        return uploadFileName;  
    }  
  
    public void setUpload(File upload) {  
        this.upload = upload;  
    }  
  
    public void setUploadContentType(String uploadContentType) {  
        this.uploadContentType = uploadContentType;  
    }  
  
    public void setUploadFileName(String uploadFileName) {  
        this.uploadFileName = uploadFileName;  
    }  
  
    public String execute() {  
        try {        	 
             File theFile = new File(this.getUploadFileName());  
             FileUtils.copyFile(upload,theFile);
             
             Archivo archivo = new Archivo(theFile);
        	 Usuario usuario = (Usuario) sesion.get("usuario");
        	 this.editor = usuario.getLogin();
//             System.out.println("Titulo: " + this.titulo);
//             System.out.println("Editor: " + this.editor);
//             System.out.println("Size: " + theFile.length());
//             System.out.println("Articulo: " + this.articulo);
        	 
             this.informe = new Informe(this.titulo, this.editor,  archivo.getBytes(), this.articulo); // crear informe en base de datos
             this.informe = new Informe(this.titulo, this.editor); // recuperar toda la informacion del informe y su autor
             
             Articulo art = new Articulo(this.informe.getArticulo(), this.informe.getAutor()); // recuperar el articulo
             Articulo temp = new Articulo(art.getTitulo(), art.getAutor(), art.getTemas(), art.getArchivo(), "informe", art.getNrevisiones()); // cambiar su estado
             
             String concepto = art.getTitulo() + ": Informe de revisión finalizado.";
             String mensaje = "Estimado " + art.getAutor() + ".\nEl informe final de revisión de su artículo con título " + art.getTitulo() + " ya está disponible para su descarga.";
             Email email = new Email(this.informe.getAutor(), concepto, mensaje);
             
             System.out.println("Destinatario "+this.informe.getAutor()+"\nConcepto "+concepto+"\nMensaje "+mensaje);           
             
        } catch (Exception ex) {  
            ex.printStackTrace();  
        }  
        return SUCCESS;  
    }  

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}
}