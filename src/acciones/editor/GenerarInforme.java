package acciones.editor;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import dominio.Editor;
import dominio.Usuario;

public class GenerarInforme extends GestionarInforme implements SessionAware{	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private InputStream inputStream;
	private String titulo;
	private String articulo;
	
	public String getArticulo() {
		return articulo;
	}
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	public Map<String, Object> getSesion() {
		return sesion;
	}
	public void setSesion(Map<String, Object> sesion) {
		this.sesion = sesion;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String execute(){
		try{
			Usuario usuario = (Usuario) sesion.get("usuario");			
			Editor editor = new Editor(usuario);
			inputStream = new ByteArrayInputStream(editor.consultarInforme(titulo).getInforme());

			return SUCCESS;			
		}catch(Exception e){
			addActionError("¡Nombre de Informe erróneo!");
			return ERROR;
		}
	}
	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
	

