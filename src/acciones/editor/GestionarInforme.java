package acciones.editor;
import java.util.Vector;

import com.opensymphony.xwork2.ActionSupport;
import dominio.Informe;

public class GestionarInforme extends ActionSupport{
	private static final long serialVersionUID = 1L;
	protected String titulo;
	protected String editor;
	protected Informe informe;
	protected String articulo;
	
	public String getArticulo() {
		return articulo;
	}
	public void setArticulo(String articulo) {
		this.articulo = articulo;
	}
	protected Vector<Informe> informes;
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public Informe getInforme() {
		return informe;
	}
	public void setInforme(Informe informe) {
		this.informe = informe;
	}
	public Vector<Informe> getInformes() {
		return informes;
	}
	public void setInformes(Vector<Informe> informes) {
		this.informes = informes;
	}
	
	
}
