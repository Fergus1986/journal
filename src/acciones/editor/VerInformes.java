package acciones.editor;

import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import dominio.Editor;
import dominio.Usuario;

public class VerInformes extends GestionarInforme implements SessionAware{
	
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	
	public String execute() {
					
		try{
			
			Usuario usuario = (Usuario) sesion.get("usuario");
//			System.out.println("Editor: " + usuario.getLogin());
			Editor editor = new Editor(usuario);
			editor.consultarInformes();
			this.informes = editor.getInformes();
//			System.out.println("Informes recuperados en la clase VerInformes "+informes.toString());
			return SUCCESS;			
			
		}catch(Exception e){
			addActionError("¡Nombre de Informe erróneo!");
			return ERROR;
		}
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		this.sesion=arg0;
		
	}

}
