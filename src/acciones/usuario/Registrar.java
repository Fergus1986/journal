package acciones.usuario;

import util.Email;
import util.cifrarPass;
import dominio.Usuario;


public class Registrar extends GestionarUsuario{
	
	private static final long serialVersionUID = 1L;
	
	
	public String execute() {
		
		try {
			String concepto ="Registro de usuario en Journal";
			String mensaje="Registro completado en Journal!!\n" +
					"\nUsuario: " + this.getUsuario().getLogin() + 
					"\nPassword: " + this.getUsuario().getSin_cifrar() + "\n";
//			System.out.println("Mensaje "+mensaje);
			Email email = new Email(this.getUsuario().getLogin(), concepto, mensaje);
			System.out.println("Destinatario "+this.getUsuario().getLogin()+"\nConcepto "+concepto+"\nMensaje "+mensaje);
			
						
			return SUCCESS;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
    }
	public void validate(){
		try {
			this.setUsuario(new Usuario(this.getLogin(),true));
		} catch (Exception e) {
			addFieldError("login", "¡El usuario ya existe!");
		}
	}
}
