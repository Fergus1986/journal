package acciones.usuario;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;


import util.cifrarPass;
import dominio.Usuario;

public class ModificarUsuario extends GestionarUsuario implements SessionAware {

	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;
	private String new_pass1, new_pass2, nombre, apellido;
	
	public String execute(){
		try {
			cifrarPass cifrado = new cifrarPass(new_pass1);
			usuario.setNombre(nombre);
			usuario.setApellido(apellido);
			usuario.setPass(cifrado.getCifrado());
			usuario.modificar();
		} catch(Exception e){
			return ERROR;
		}
		return SUCCESS;
	}
	
	public void validate(){
		this.setUsuario((Usuario)this.sesion.get("usuario"));
		cifrarPass cifrado = new cifrarPass(this.getPassword());		
		if(!(new_pass1.equals(new_pass2))){
			addFieldError("new_pass1", "Las contraseñas no coinciden");
		}
		if(!(cifrado.getCifrado().equals(usuario.getPass()))){
			addFieldError("password", "Contraseña errónea");
		}		
	}
	
	public String getNew_pass1() {
		return new_pass1;
	}

	public void setNew_pass1(String new_pass1) {
		this.new_pass1 = new_pass1;
	}

	public String getNew_pass2() {
		return new_pass2;
	}

	public void setNew_pass2(String new_pass2) {
		this.new_pass2 = new_pass2;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.setSesion(arg0);
		
	}

	public Map<String, Object> getSesion() {
		return sesion;
	}

	public void setSesion(Map<String, Object> sesion) {
		this.sesion = sesion;
	}
}
