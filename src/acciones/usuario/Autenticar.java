package acciones.usuario;

import java.util.Map;

import util.cifrarPass;

import org.apache.struts2.interceptor.SessionAware;

import dominio.Usuario;
import excepciones.UsuarioErroneoException;

public class Autenticar extends GestionarUsuario implements SessionAware {

	private static final long serialVersionUID = 1L;
	private Map<String, Object> sesion;

	public String execute() {

		try {
			this.getSesion().put("usuario", this.getUsuario());

			return SUCCESS;

		} catch (Exception e) {
			addActionError("¡Nombre de Usuario o Contraseña erróneos!");
			return ERROR;
		}
	}

	public void validate() {
		try {
			cifrarPass cifrado = new cifrarPass(this.getPassword());
			this.setPassword(cifrado.getCifrado());
			this.setUsuario(new Usuario(this.getLogin(), this.getPassword()));
		} catch (Exception e) {
			addFieldError("login", "¡Nombre de Usuario o Contraseña erróneos!");
		}
	}

	public Map<String, Object> getSesion() {
		return sesion;
	}

	public void setSesion(Map<String, Object> sesion) {
		this.sesion = sesion;
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		this.setSesion(arg0);

	}
}
