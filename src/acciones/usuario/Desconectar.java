package acciones.usuario;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;


import com.opensymphony.xwork2.ActionContext;


public class Desconectar extends GestionarUsuario {
	
	private static final long serialVersionUID = 1L;
	
	public String execute() {
		
		try{
			
			ActionContext contexto = ActionContext.getContext();
			HttpServletRequest peticion = (HttpServletRequest) contexto.get(ServletActionContext.HTTP_REQUEST);
			HttpSession sesion = peticion.getSession();
			sesion.invalidate();
			
			return SUCCESS;
			
		}catch(Exception e){
			addActionError("¡Error al desconectar!");
			return ERROR;
		}
	}
}
