package acciones.usuario;

import com.opensymphony.xwork2.ActionSupport;

import dominio.Usuario;

public class GestionarUsuario extends ActionSupport{
	
	private static final long serialVersionUID = 1L;
	
	protected String login;
	protected String password;
	protected Usuario usuario;
		
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
