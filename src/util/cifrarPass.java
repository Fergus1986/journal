package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class cifrarPass {
	
	private String cifrado;
	
	public cifrarPass(){}
	
	public cifrarPass(String password){
		
		this.cifrado="";
		this.cifrar(password);
	}
	
	private void cifrar(String password){
		
		try {
			MessageDigest algoritmo = MessageDigest.getInstance("MD5");
			algoritmo.reset();
			algoritmo.update(password.getBytes());
			byte [] md5 = algoritmo.digest();
			
			StringBuffer buffer = new StringBuffer();
			
			for(int i=0; i< md5.length; i++){
				buffer.append(Integer.toString((md5[i] & 0xff) + 0x100, 16).substring(1));	
			}
			
			this.cifrado = buffer.toString();
			
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
//		System.out.println("MD5: " + this.cifrado);
		
	}
	
	public String getCifrado(){
		return this.cifrado;
	}
	
	public String generateString () {
		Random rng = new Random();
		int length = rng.nextInt(8)+12;
		String characters = "abcdefghijklmnopqrstuvwxyz0123456789!?*_ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    char[] text = new char[length];
	    for (int i = 0; i < length; i++) {
	        text[i] = characters.charAt(rng.nextInt(characters.length()));
	    }
	    return new String(text);
	}

}
