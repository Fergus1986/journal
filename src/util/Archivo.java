package util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Archivo {
	
	FileInputStream flujo;
	File fichero;
	byte[] bytes;
	

	public Archivo(File archivo) throws FileNotFoundException{
		this.flujo = new FileInputStream(archivo);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = flujo.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
//				Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
//                System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException ex) {
//            Logger.getLogger(genJpeg.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.bytes = bos.toByteArray();
	}
	
	public byte[] getBytes(){
		return this.bytes;
	}
	
	public Archivo(byte[] bytes, String nombre) throws IOException{
		
		this.fichero = new File(nombre);
        FileOutputStream fos = new FileOutputStream(this.fichero);
        fos.write(bytes);
        fos.flush();
        fos.close();		
	}
	
	public File getFichero(){
		return this.fichero;
	}

}
