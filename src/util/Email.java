package util;


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class Email {
	
	private MimeMessage mensaje;
	private String host, usuario, password;
	private String destinatario, concepto, contenido;
	public MimeMessage getMensaje() {
		return mensaje;
	}

	public void setMensaje(MimeMessage mensaje) {
		this.mensaje = mensaje;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Properties getPropiedades() {
		return propiedades;
	}

	public void setPropiedades(Properties propiedades) {
		this.propiedades = propiedades;
	}

	public Session getSesion() {
		return sesion;
	}

	public void setSesion(Session sesion) {
		this.sesion = sesion;
	}

	private Properties propiedades;
	private Session sesion;
	
	public Email(String destinatario,
			String concepto,
			String contenido){
		
		this.setHost("smtp.gmail.com");
		this.setUsuario("JournalTechnologiesInc@gmail.com");
		this.setPassword("journalpass");
		
		this.destinatario = destinatario;
		this.concepto = concepto;
		this.contenido = contenido;
		
		this.generarEmail();
		
	}
	
	private void generarEmail(){
				
        // Propiedades SMTP 
        this.propiedades = new Properties();
        propiedades.put("mail.smtps.auth",  "true");
        this.sesion = Session.getDefaultInstance(propiedades);
        this.mensaje = new MimeMessage(sesion);
        
        try {
			mensaje.setSubject(concepto);
			mensaje.setText(contenido);
			mensaje.setFrom(new InternetAddress(this.getUsuario()));
			mensaje.addRecipient(Message.RecipientType.TO,
					new InternetAddress(this.getDestinatario()));
			Transport t = sesion.getTransport("smtps");
			t.connect(this.getHost(), this.getUsuario(), this.getPassword());
			t.sendMessage(mensaje, mensaje.getAllRecipients());
			t.close();

			
		} catch (MessagingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();			
		}
		
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
