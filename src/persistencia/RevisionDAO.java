package persistencia;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.sql.rowset.serial.SerialBlob;

import dominio.Revision;
import dominio.Articulo;
import excepciones.RevisionNuevaException;
import excepciones.RevisionErroneaException;
import excepciones.NoHayConexionException;

public class RevisionDAO {

	public static ArrayList<String> getAceptadas(String revisor) throws Exception { 
		ArrayList<String> revisiones = new ArrayList<String>();
		
		String SQL = "SELECT articulo FROM revision WHERE revisor=? AND revision.estado='aceptada'";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, revisor);
			ResultSet r = p.executeQuery();

			while (r.next()) {
				revisiones.add(r.getString(1));
			}

		} catch (Exception e) {
			throw new RevisionErroneaException("", revisor);
		} finally {
			BD.close();
		}
		
		return revisiones;
	}
	
	
	public static Vector<Revision> getRevisiones(String revisor, String estado)
			throws RevisionErroneaException, SQLException,
			NoHayConexionException {

		Vector<Revision> revisiones = new Vector<Revision>();
		Revision revision = null;
		Articulo articulo = null;
		String SQL = "SELECT * FROM revision, articulo  WHERE revision.articulo = articulo.titulo"
				+ " AND revision.revisor = ?" + " AND revision.estado = ?";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, revisor);
			p.setString(2, estado);
			ResultSet r = p.executeQuery();

			while (r.next()) {

				revision = new Revision();
				articulo = new Articulo();

				revision.setRevisor(r.getString(2));
				revision.setEstado(r.getString(3));

				// Revision PDF
				if (estado.equals("terminada")) {
					revision.setBaremo(r.getInt(4));
					byte[] temporal = null;
					int s = (int) r.getBlob(5).length();
					temporal = r.getBlob(5).getBytes(1, s);
					revision.setArchivo(temporal);
				}

				articulo.setTitulo(r.getString(6));
				articulo.setAutor(r.getString(7));
				articulo.setTemas(r.getString(8));

				// Articulo PDF
				byte[] temporal2 = null;
				int s2 = (int) r.getBlob(9).length();
				temporal2 = r.getBlob(9).getBytes(1, s2);
				articulo.setArchivo(temporal2);

				articulo.setEstado(r.getString(10));

				// Anadir archivo a revision
				revision.setArticulo(articulo);

				// Anadir revision a lista de revisiones
				revisiones.add(revision);

			}

		} catch (Exception e) {
			throw new RevisionErroneaException("", revisor);
		} finally {
			BD.close();
		}

		return revisiones;
	}

	public static Revision getRevision(String tituloArticulo, String revisor)
			throws RevisionErroneaException, SQLException,
			NoHayConexionException {

//		System.out.println("Revision: getRevision: " + tituloArticulo + " "
//				+ revisor);

		Revision revision = null;
		Articulo articulo = null;
		String SQL = "SELECT * FROM revision, articulo WHERE revision.articulo=? AND"
				+ " revision.revisor=? AND revision.articulo = articulo.titulo";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, tituloArticulo);
			p.setString(2, revisor);
			ResultSet r = p.executeQuery();

			r.next();

//			System.out.println("Consulta satisfactoria");

			revision = new Revision();
			articulo = new Articulo();

			articulo.setTitulo(r.getString(1));

			// Articulo PDF
			byte[] temporal2 = null;
			int s2 = (int) r.getBlob(9).length();
			temporal2 = r.getBlob(9).getBytes(1, s2);
			articulo.setArchivo(temporal2);

			revision.setRevisor(r.getString(2));
			revision.setBaremo(r.getInt(4));

			revision.setArticulo(articulo);

			// Revision PDF
			if (r.getString(3).equals("terminada")) {
				byte[] temporal = null;
				int s = (int) r.getBlob(5).length();
				temporal = r.getBlob(5).getBytes(1, s);

				revision.setArchivo(temporal);
//				System.out.println("Titulo Revision:"
//						+ revision.getArticulo().getTitulo());
//				System.out.println("Contenido Revision: "
//						+ revision.getArchivo().toString());
			}
		} catch (Exception e) {
			throw new RevisionErroneaException(tituloArticulo, revisor);
		} finally {
			BD.close();
		}

		return revision;
	}

	public static void nuevaRevision(String tituloArticulo, String revisor,	String estado)
			throws RevisionNuevaException, SQLException, NoHayConexionException {

		String SQL = "REPLACE INTO revision (articulo, revisor, estado, baremo, archivo) VALUES (?,?,?, NULL, NULL)";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, tituloArticulo);
			p.setString(2, revisor);
			p.setString(3, estado);
			p.executeUpdate();

		} catch (SQLException e2) {
			System.out.println(e2.toString());

		} catch (Exception e) {
			throw new RevisionNuevaException(tituloArticulo, revisor);

		} finally {
			BD.close();
		}
	}

	public static void nuevaRevision(String tituloArticulo, String revisor,
			String estado, int baremo, byte[] revision)
			throws RevisionNuevaException, SQLException, NoHayConexionException {

		String SQL = "REPLACE INTO revision (articulo, revisor, estado, baremo, archivo) VALUES (?,?,?,?,?)";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, tituloArticulo);
			p.setString(2, revisor);
			p.setString(3, estado);
			p.setInt(4, baremo);
			Blob blob = new SerialBlob(revision);
			p.setBlob(5, blob);

			p.executeUpdate();

		} catch (SQLException e2) {
			System.out.println(e2.toString());

		} catch (Exception e) {
			throw new RevisionNuevaException(tituloArticulo, revisor);

		} finally {
			BD.close();
		}
	}
	
	public static void rechazarRevision(String tituloArticulo, String revisor) {
		
		String SQL = "DELETE FROM revision WHERE articulo=? AND revisor=?";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, tituloArticulo);
			p.setString(2, revisor);
			p.executeUpdate();
			BD.close();
		} catch (SQLException e2) {
			System.out.println(e2.toString());
		} catch (NoHayConexionException e) {
			System.out.println(e.toString());
		}
	}

}
