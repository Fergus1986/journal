package persistencia;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.sql.rowset.serial.SerialBlob;

import dominio.Articulo;
import excepciones.ArticuloNuevoException;
import excepciones.NoHayConexionException;
import excepciones.ArticuloErroneoException;

public class ArticuloDAO {

	public static Vector<Articulo> getArticulos(String autor)
			throws ArticuloErroneoException, SQLException,
			NoHayConexionException {

		Vector<Articulo> articulos = new Vector<Articulo>();
		Articulo articulo = null;
		String SQL = "SELECT articulo.titulo,autor,temas,articulo.archivo,estado,nrevisiones,informe.titulo " +
				"FROM articulo LEFT OUTER JOIN informe ON informe.articulo=articulo.titulo " +
				"WHERE autor=?";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, autor);
			ResultSet r = p.executeQuery();

			while (r.next()) {

				articulo = new Articulo();
				articulo.setTitulo(r.getString(1));
				articulo.setAutor(r.getString(2));
				articulo.setTemas(r.getString(3));
				
				byte[] temporal = null;
				int s = (int) r.getBlob(4).length();
				temporal = r.getBlob(4).getBytes(1, s);
				
				articulo.setArchivo(temporal);
				
				articulo.setEstado(r.getString(5));
				articulo.setNrevisiones(r.getInt(6));
				
				String informe = "";
				if (articulo.getEstado().equals("informe")) {
					informe = r.getString(7);
				}
				
				articulo.setInforme(informe);
				articulos.add(articulo);
			}

		} catch (Exception e) {
			throw new ArticuloErroneoException("", autor);
		} finally {
			BD.close();
		}

		return articulos;
	}
	
	public static ArrayList<String> getArticulosInforme() throws Exception {
		ArrayList<String> articulos = new ArrayList<String>();
		String SQL = "SELECT articulo.titulo FROM articulo JOIN " +
				"(SELECT count(revision.articulo) AS count_art FROM articulo,revision WHERE " +
				"revision.articulo=articulo.titulo AND revision.estado='terminada' AND articulo.estado='revision' " +
				"GROUP BY revision.articulo) AS counting " +
				"WHERE articulo.nrevisiones=counting.count_art AND articulo.nrevisiones>1";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			ResultSet r = p.executeQuery();

			while (r.next()) {

				articulos.add(r.getString(1));
				System.out.println("Articulo: " + r.getString(1));
			}
		} catch (Exception e) {
			throw new ArticuloErroneoException("", "");
		} finally {
			BD.close();
		}

		return articulos;
	}
	
	public static ArrayList<String> getArticulosEstado(String estado)
			throws Exception {

		ArrayList<String> articulos = new ArrayList<String>();
		String SQL = "SELECT * FROM articulo WHERE articulo.estado=? AND articulo.nrevisiones<5";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, estado);
			ResultSet r = p.executeQuery();

			while (r.next()) {
				
				articulos.add(r.getString(1));
			}
//			System.out.println("Articulos "+articulos.size());
		} catch (Exception e) {
			throw new ArticuloErroneoException(estado,"");
		} finally {
			BD.close();
		}

		return articulos;
	}

	public static Articulo getArticulo(String titulo, String autor)
			throws ArticuloErroneoException, SQLException,
			NoHayConexionException {

		Articulo articulo = null;
		String SQL = "SELECT articulo.titulo,autor,temas,articulo.archivo,estado,nrevisiones,informe.titulo " +
				"FROM articulo LEFT OUTER JOIN informe ON informe.articulo=articulo.titulo " +
				"WHERE articulo.titulo=? AND autor=?";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, titulo);
			p.setString(2, autor);
			ResultSet r = p.executeQuery();

			r.next();

			articulo = new Articulo();
			articulo.setTitulo(r.getString(1));
			articulo.setAutor(r.getString(2));
			articulo.setTemas(r.getString(3));
			
			byte[] temporal = null;
			int s = (int) r.getBlob(4).length();
			temporal = r.getBlob(4).getBytes(1, s);
			
			articulo.setArchivo(temporal);
			articulo.setEstado(r.getString(5));
			articulo.setNrevisiones(r.getInt(6));
			
			String informe = "";
			if (articulo.getEstado().equals("informe")) {
				informe = r.getString(7);
			}
			
			articulo.setInforme(informe);
			
//			System.out.println("ArticuloDAO" + Arrays.toString(temporal));

		} catch (Exception e) {
			throw new ArticuloErroneoException(titulo, autor);
		} finally {
			BD.close();
		}

		return articulo;
	}

	public static void nuevoArticulo(String titulo,
			String autor,
			String temas,
			byte[] archivo,
			String estado,
			int nrevisiones) throws ArticuloNuevoException,
			SQLException, NoHayConexionException {

		String SQL = "REPLACE INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES (?,?,?,?,?,?)";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, titulo);
			p.setString(2, autor);
			p.setString(3, temas);
			
			Blob blob = new SerialBlob(archivo);	
			
			p.setBlob(4, blob);
			p.setString(5, estado);
			p.setInt(6, nrevisiones);
			p.executeUpdate();
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
			
		} catch (Exception e) {
			throw new ArticuloNuevoException(titulo, autor);
			
		} finally {
			BD.close();
		}
	}

	public static void modificarArticulo(String tituloArticulo) throws NoHayConexionException, SQLException {
		String SQL="UPDATE articulo SET nrevisiones=nrevisiones+1 WHERE articulo.titulo=? AND articulo.nrevisiones<5"; 
		Conexion BD = null;
		PreparedStatement p = null;
		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, tituloArticulo);
			p.executeUpdate();
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
		} finally {
			BD.close();
		}
	
	}
}
