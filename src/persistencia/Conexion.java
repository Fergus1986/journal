package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Conexion {
	
	private Connection bd;
	protected String url="jdbc:odbc:journal";
	private int id;
	
	public Conexion(int id) throws SQLException {
		this.id=id;
		this.url="jdbc:mysql://localhost:3306/journal";
		this.bd=DriverManager.getConnection(url, "root", "root");
	}
	
	public void close() throws SQLException {
		synchronized (this) {
			Broker.get().libera(this);
		}
	}

	public int getId() {
		return id;
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return this.bd.prepareStatement(sql);
	}

}
