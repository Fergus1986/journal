package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import excepciones.NoHayConexionException;
import excepciones.UsuarioErroneoException;
import excepciones.UsuarioNuevoException;
import dominio.Articulo;
import dominio.Usuario;
import util.cifrarPass;

public class UsuarioDAO {
	
	public static void getEditor(Usuario usuario) throws UsuarioErroneoException, SQLException, NoHayConexionException {
		
		String SQL = "SELECT * FROM usuario WHERE rol='editor'";
		Conexion BD=null;
		PreparedStatement p=null;
		
		try{
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			ResultSet r = p.executeQuery();
			r.next();
			usuario.setLogin(r.getString(1));
			usuario.setPass(r.getString(2));
			usuario.setRol(r.getString(3));
			usuario.setNombre(r.getString(4));
			usuario.setApellido(r.getString(5));
			
		}catch (Exception e){
			throw new UsuarioErroneoException(usuario.getLogin(), usuario.getPass());
		}finally{
			BD.close();
		}
	}
	
	//Este coge los usuarios según el rol
	public static ArrayList<String> getUsuarios(String rol) throws Exception{
		
		ArrayList<String> usuarios = new ArrayList<String> ();
		
		String SQL = "SELECT * FROM usuario WHERE rol=?";
		Conexion BD=null;
		PreparedStatement p=null;	
		try{
			
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1,rol);
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				
				usuarios.add(r.getString(1));
			}
			
//			System.out.println("UsuarioDAO: Usuarios = " + usuarios.size());

			
		}catch (Exception e){
			throw new UsuarioErroneoException("","");
		}finally{
			BD.close();
		}		
		
		return usuarios;
		
	}
	
	//Este sólo me devuelve los revisores que tengan asignados menos de 10 artículos para revisar
	public static ArrayList<String> getUsuariosRevisores() throws Exception{
		
		ArrayList<String> usuarios = new ArrayList<String> ();
		
		String SQL = "SELECT * FROM usuario WHERE rol='revisor' AND nrevisiones<10";
		Conexion BD=null;
		PreparedStatement p=null;	
		try{
			
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			ResultSet r = p.executeQuery();
			
			while (r.next()) {
				
				usuarios.add(r.getString(1));
			}
			
//			System.out.println("UsuarioDAO: Usuarios = " + usuarios.size());

			
		}catch (Exception e){
			throw new UsuarioErroneoException("","");
		}finally{
			BD.close();
		}		
		
		return usuarios;
		
	}
	public static Usuario getUsuario(String login, String pass) 
			throws
			UsuarioErroneoException,
			SQLException,
			NoHayConexionException{
		
		Usuario usuario = null;
		String SQL = "SELECT * FROM usuario WHERE login=? AND pass=?";
		Conexion BD=null;
		PreparedStatement p=null;
		
		try{
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1,login);
			p.setString(2,pass);
			ResultSet r = p.executeQuery();
			
			r.next();
			usuario = new Usuario();
			usuario.setLogin(r.getString(1));
			usuario.setPass(r.getString(2));
			usuario.setRol(r.getString(3));
			usuario.setNombre(r.getString(4));
			usuario.setApellido(r.getString(5));
			usuario.setNrevisiones(r.getInt(6));
						
			
		}catch (Exception e){
			throw new UsuarioErroneoException(login, pass);
		}finally{
			BD.close();
		}
		
		return usuario;
	}
	
	public static String insertarNuevoUsuario(String login, String rol)
			throws
			SQLException,
			NoHayConexionException,
			UsuarioNuevoException{
		
		cifrarPass cifrado = new cifrarPass();
		String pass = cifrado.generateString();
		cifrado = new cifrarPass(pass);
		
//		System.out.println("pass generada: " + pass + "\n cifrada: " + cifrado.getCifrado());
		
		String SQL = "INSERT INTO usuario (login, pass, rol, nombre, apellido, nrevisiones) VALUES(?,?,?, NULL, NULL, 0)";
		Conexion BD=null;
		PreparedStatement p=null;
		
		try{
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, login);
			p.setString(2, cifrado.getCifrado());
			p.setString(3, rol);
			p.executeUpdate();	
			
		}catch (Exception e){
			throw new UsuarioNuevoException(login);
		}finally{
			BD.close();			
		}		
		return pass;
	}
	
	public static String recuperarPass(String login)
			throws
			UsuarioErroneoException,
			SQLException,
			NoHayConexionException{
		
		String pass="";
		
		String SQL = "SELECT pass FROM usuario WHERE login=?";
		Conexion BD=null;
		PreparedStatement p=null;
		
		try{
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, login);
			
			ResultSet r = p.executeQuery();
			r.next();
			
			pass = r.getString(1);
			
		}catch (Exception e){
			throw new UsuarioErroneoException(login, "");
		}finally{
			BD.close();
		}		
		return pass;
		
	}
	
	public static void actualizarUsuario(String login, String pass, String rol, String nombre, String apellido, int nrevisiones) 
			throws SQLException,
			NoHayConexionException,
			UsuarioNuevoException{
			
		String SQL = "REPLACE INTO usuario (login, pass, rol, nombre, apellido, nrevisiones) VALUES(?,?,?,?,?,?)";
		Conexion BD=null;
		PreparedStatement p=null;
		
		try{
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, login);
			p.setString(2, pass);
			p.setString(3, rol);
			p.setString(4, nombre);
			p.setString(5, apellido);
			p.setInt(6, nrevisiones);
			p.executeUpdate();	
			
		}catch (Exception e){
			throw new UsuarioNuevoException(login);
		}finally{
			BD.close();
		}		
	}

	public static void modificarRevisor(String revisor) throws NoHayConexionException, SQLException {
		String SQL="UPDATE usuario SET nrevisiones=nrevisiones+1 WHERE usuario.login=? AND usuario.nrevisiones<10"; 
		Conexion BD = null;
		PreparedStatement p = null;
		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, revisor);
			p.executeUpdate();
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
		} finally {
			BD.close();
		}
		
	}
	
	public static void revisionTerminada(String revisor) throws NoHayConexionException, SQLException {
		String SQL="UPDATE usuario SET nrevisiones=nrevisiones-1 WHERE usuario.login=?"; 
		Conexion BD = null;
		PreparedStatement p = null;
		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, revisor);
			p.executeUpdate();
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
		} finally {
			BD.close();
		}
		
	}
	
	public static int revisionesRevisor(String revisor) throws NoHayConexionException, SQLException {
		String SQL="SELECT nrevisiones FROM usuario WHERE usuario.login=?"; 
		Conexion BD = null;
		PreparedStatement p = null;
		int nrevisiones=0;
		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, revisor);
			
			ResultSet r = p.executeQuery();
			r.next();
			
			nrevisiones = r.getInt(1);
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
		} finally {
			BD.close();
		}
		return nrevisiones;
		
	}



}
