package persistencia;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Vector;

import javax.sql.rowset.serial.SerialBlob;

import dominio.Informe;
import excepciones.InformeErroneoException;
import excepciones.InformeNuevoException;
import excepciones.NoHayConexionException;

public class InformeDAO {

	public static Vector<Informe> getInformes(String editor)
			throws InformeErroneoException, SQLException,
			NoHayConexionException {

		Vector<Informe> informes = new Vector<Informe>();
		Informe informe = null;
		String SQL = "SELECT informe.titulo, informe.editor,informe.archivo, informe.articulo, articulo.autor,articulo.archivo FROM informe,articulo WHERE editor=? AND informe.articulo=articulo.titulo";
		
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, editor);
			ResultSet r = p.executeQuery();

			while (r.next()) {

				informe = new Informe();
				informe.setTitulo(r.getString(1));
				informe.setEditor(r.getString(2));
				
				byte[] temporal = null;
				int s = (int) r.getBlob(3).length();
				temporal = r.getBlob(3).getBytes(1, s);
				informe.setArticulo(r.getString(4));
				informe.setAutor(r.getString(5));
				informe.setInforme(temporal);
				
				informes.add(informe);
//				System.out.println("Informes recuperados hasta el momento "+informes.toString());
			}

		} catch (Exception e) {
			throw new InformeErroneoException("", editor);
		} finally {
			BD.close();
		}

		return informes;
	}

	public static Informe getInforme(String titulo, String editor)
			throws InformeErroneoException, SQLException,
			NoHayConexionException, InformeErroneoException {

		Informe informe= null;
		String SQL = "SELECT informe.titulo, informe.editor,informe.archivo, informe.articulo, articulo.autor,articulo.archivo FROM informe,articulo WHERE informe.titulo=? AND informe.editor=? AND informe.articulo=articulo.titulo";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, titulo);
			p.setString(2, editor);
			ResultSet r = p.executeQuery();
			r.next();

			informe = new Informe();
			informe.setTitulo(r.getString(1));
			informe.setEditor(r.getString(2));
			byte[] temporal = null;
			int s = (int) r.getBlob(3).length();
			temporal = r.getBlob(3).getBytes(1, s);
			informe.setInforme(temporal);
			informe.setArticulo(r.getString(4));
			informe.setAutor(r.getString(5));
			byte[] temporal2 = null;
			int s1 = (int) r.getBlob(6).length();
			temporal2 = r.getBlob(6).getBytes(1, s1);
			informe.setArchivo(temporal2);
			
			
//			System.out.println("InformeDAO" + Arrays.toString(temporal));
//			System.out.println("InformeDAO" + Arrays.toString(temporal2));

		} catch (Exception e) {
			throw new InformeErroneoException(titulo, editor);
		} finally {
			BD.close();
		}

		return informe;
	}

	public static void nuevoInforme(String titulo,
			String editor,
			byte[] informe,
			String articulo) throws InformeNuevoException,
			SQLException, NoHayConexionException {

		String SQL = "INSERT INTO informe (titulo, editor, archivo, articulo) VALUES (?,?,?,?)";
		Conexion BD = null;
		PreparedStatement p = null;

		try {
			BD = Broker.get().getBD();
			p = BD.prepareStatement(SQL);
			p.setString(1, titulo);
			p.setString(2,editor);
			Blob blob = new SerialBlob(informe);	
			
			p.setBlob(3, blob);
			p.setString(4,articulo);
			p.executeUpdate();
			
		}catch(SQLException e2){
			System.out.println(e2.toString());
			
		} catch (Exception e) {
			throw new InformeNuevoException(titulo, editor);
			
		} finally {
			BD.close();
		}
	}
}
