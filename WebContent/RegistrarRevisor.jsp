<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>

	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Registro de Nuevo Revisor</div>
			<s:form action="RegistrarRevisor" method="post" validation="true">
				<s:textfield name="login" label="Email" />
				<s:submit value="Registrar" />
			</s:form>
		</div>

		<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>

			<div id="opcion">
				<a href="ModificarUsuario.jsp">Modificar perfil</a>
			</div>

			<div id="opcion">
				<s:form action="ConsultarArticulos">
					<s:submit value="Subir Informe" />
				</s:form>
			</div>

			<div id="opcion">
				<s:form action="ConsultarRevisores">
					<s:submit value="Asignar Revisores" />
				</s:form>
			</div>

			<div id="opcion">
				<s:form action="VerInformes">
					<s:submit value="Ver Informes" />
				</s:form>
			</div>
			<div id="opcion">
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>
			</div>
		</div>
	</div>

	<div id="pie">
			<h3>Journal Technologies</h3>
		</div>
</body>
</html>