<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>

		<div id="cabecera">
			<h1>Journals</h1>
		</div>

		<div id="container" class="cuerpo">

			<div id="izquierda" class="altura">

				<div id="seccion">Bienvenido Invitado</div>
<%--  				<s:fielderror cssClass="error"/> --%>
				<s:form action="Autenticar" validation="true" method="post">
					<s:textfield name="login" label="Usuario"/>
					<s:password name="password" label="Contraseña"/>
					<s:submit value="Entrar" cssClass="boton"/>
				</s:form>
				
			</div>
			
			

			<div id="derecha" class="altura"></div>

		</div>

		<div id="pie">
			<h3>Journal Technologies</h3>
		</div>
</body>
</html>