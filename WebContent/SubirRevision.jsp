<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>
	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Subir Revisión</div>

			<s:form action="SubirRevision" method="post" validation="true"
				enctype="multipart/form-data">
				<s:select name="titulo" list="%{#session.revisiones}"
					headerValue=" -- Seleccione -- " label="Título del artículo" />
				<s:select name="baremo" list="%{#session.baremos}"
					headerValue=" -- Seleccione -- " label="Baremo" />
				<s:file name="upload" label="Fichero de revisión" />
				<s:submit value="Subir" />
			</s:form>

		</div>

		<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>
			
			<div id="opcion">
				<a href="ModificarUsuario.jsp">Modificar perfil</a>
			</div>
			
			<div id="opcion" >
			<s:form action="VerRevisiones">
					<s:submit value="Ver Revisiones" />
				</s:form>
			</div>

			<div id="opcion" >
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>
			</div>
		</div>
	</div>
	<div id="pie">
		<h3>Journal Technologies</h3>
	</div>

</body>
</html>