<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>
	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Revisiones Asignadas</div>

			<table id="revisiones" align="center">
				<tr>
					<th>Articulo</th>
					<th>Temas</th>
					<th>Decisión</th>
				</tr>
				<s:iterator value="revisionesAsignadas" var="rev" status="revisionesEstado">

						<s:if test="#revisionesEstado.even == true">
							<tr class="par">
						</s:if>
						<s:else>
							<tr class="impar">
						</s:else>
						<td><s:property value="getTituloArticulo()" /></td>

						<td><s:property value="getTemasArticulo()" /></td>

						<td><s:form action="AceptarRevision">
							<s:hidden name="tituloArticulo" value="%{#rev.getTituloArticulo()}" />
							<s:submit value="Aceptar"/> <%-- validation="true"--%>
							
							<%-- Aqui en Aceptar revision deberiamos validar o no y sacarle al revisor un mensaje tipo
							no puedes aceptar mas revisiones --%>
							
						</s:form>
						<s:form action="RechazarRevision">
							<s:hidden name="tituloArticulo" value="%{#rev.getTituloArticulo()}" />
							<s:submit value="Rechazar" />
						</s:form></td>

						</tr>

				</s:iterator>
			</table>

			<p></p>

			<div id="seccion">Revisiones Aceptadas</div>

			<table id="revisiones" align="center">
				<tr>
					<th>Articulo</th>
					<th>Archivo</th>
				</tr>
				<s:iterator value="revisionesAceptadas" var="rev" status="revisionesEstado">
											
					<s:if test="#revisionesEstado.even == true">
						<tr class="par">
					</s:if>
					<s:else>
						<tr class="impar">
					</s:else>
					<td><s:property value="getTituloArticulo()" /></td>
					
					<td>
						<s:form action="GenerarPDFArticuloRevision">
							<s:hidden name="descarga" value="%{#rev.getArticulo().getTitulo()}" />
							<s:submit value="Descargar Articulo" />
						</s:form>
					</td>
					
					</tr>
				</s:iterator>
			</table>

			<p></p>
			
			<div id="seccion">Revisiones Terminadas</div>

			<table id="revisiones" align="center">
				<tr>
					<th>Titulo</th>
					<th>Articulo</th>
					<th>Baremo</th>
					<th>Revisión</th>
				</tr>
				<s:iterator value="revisionesTerminadas" var="rev" status="revisionesEstado">
							
					<s:if test="#revisionesEstado.even == true">
						<tr class="par">
					</s:if>
					<s:else>
						<tr class="impar">
					</s:else>
					<td><s:property value="getTituloArticulo()" /></td>
					
					<td>
						<s:form action="GenerarPDFArticuloRevision">
							<s:hidden name="descarga" value="%{#rev.getArticulo().getTitulo()}" />
							<s:submit value="Descargar Articulo" />
						</s:form>
					</td>

					<td><s:property value="getBaremo()" /></td>

					<td>
						<s:form action="GenerarPDFRevision">
							<s:hidden name="descarga" value="%{#rev.getTituloArticulo()}" />
							<s:submit value="Descargar Revision"/>
						</s:form>
						
						</td>
					</tr>	
				</s:iterator>
			</table>			
			
			<p></p>

		</div>

		<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>

			<div id="opcion">
				<a href="ModificarUsuario.jsp">Modificar perfil</a>
			</div>

			<div id="opcion">
				<s:form action="ConsultarRevisiones">
					<s:submit value="Subir revisión" />
				</s:form>
			</div>
			
			<div id="opcion">
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>
			</div>

		</div>

	</div>

	<div id="pie">
		<h3>Journal Technologies</h3>
	</div>

</body>
</html>