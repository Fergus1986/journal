<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>
	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Informes</div>

			<table id="informesTabla" align="center">
				<tr>
					<th>Autor</th>
					<th>Artículo</th>
					<th>Archivo</th>
					<th>Editor</th>
					<th>Informe</th>
					<th>Archivo</th>
				</tr>
				<s:iterator value="informes" var="inf" status="informesEstado">
					<s:if test="#informesEstado.even == true">
						<tr class="par">
					</s:if>
					<s:else>
						<tr class="impar">
					</s:else>
					<td><s:property value="getAutor()" /></td>
					<td><s:property value="getArticulo()" /></td>
					<td><s:form action="GenerarPDFInformeArticulo">							
							<s:hidden name="descarga1" value="%{#inf.getTitulo()}" />
							<s:submit value="Descargar" />
						</s:form></td>
					<td><s:property value="getEditor()" /></td>
					<td><s:property value="getTitulo()" /></td>
					<td><s:form action="GenerarPDFInforme">							
							<s:hidden name="descarga" value="%{#inf.getTitulo()}" />
							<s:submit value="Descargar" />
						</s:form></td>
					</tr>
				</s:iterator>
			</table>
		</div>
		<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>

			<div id="opcion">
				<a href="ModificarUsuario.jsp">Modificar perfil</a>
			</div>

			<div id="opcion">
				<a href="RegistrarRevisor.jsp">Registrar Revisor</a>
			</div>

			<div id="opcion">
				<s:form action="ConsultarArticulos">
					<s:submit value="Subir Informe" />
				</s:form>
			</div>

			<div id="opcion">
				<s:form action="ConsultarRevisores">
					<s:submit value="Asignar Revisores" />
				</s:form>
			</div>

			<div id="opcion">
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>
			</div>
		</div>

	</div>

	<div id="pie">
		<h3>Journal Technologies</h3>
	</div>

</body>
</html>