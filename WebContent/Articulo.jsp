<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>
	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Articulos</div>

			<table id="articulos" align="center">
				<tr>
					<th>Titulo</th>
					<th>Temas</th>
					<th>Estado</th>
					<th>Archivo</th>
					<th>Informe</th>
				</tr>
				<s:iterator value="articulos" var="art" status="articulosEstado">

					<s:if test="#articulosEstado.even == true">
						<tr class="par">
					</s:if>
					<s:else>
						<tr class="impar">
					</s:else>
					<td><s:property value="getTitulo()" /></td>
					<td><s:property value="getTemas()" /></td>
					<td><s:property value="getEstado()" /></td>
					<td>
						<s:form action="GenerarPDFArticulo">
							<s:hidden name="descarga" value="%{#art.getTitulo()}" />
							<s:submit value="Descargar" />
						</s:form>
					</td>
					<td>
						<s:if test="#art.getEstado() == 'informe'">
							<s:form action="GenerarPDFArticuloInforme">
								<s:hidden name="descarga" value="%{#art.getInforme()}" />
								<s:submit value="Descargar Informe" />
							</s:form>
						</s:if>
					</td>
					</tr>
				</s:iterator>
			</table>
			
			<p></p>
			
		</div>

		<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>
			
			<div id="opcion">
				<a href="ModificarUsuario.jsp">Modificar perfil</a>
			</div>

			<div id="opcion" ><a href="SubirArticulo.jsp">Subir Articulo</a></div>

			<div id="opcion" >
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>
			</div>

		</div>

	</div>

	<div id="pie">
		<h3>Journal Technologies</h3>
	</div>

</body>
</html>
