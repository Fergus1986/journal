<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<link href="<s:url value="Estilo.css"/>" rel="stylesheet"
	type="text/css" />

<title>Journals</title>
</head>

<body>
	<div id="cabecera">
		<h1>Journals</h1>
	</div>

	<div id="container" class="cuerpo">

		<div id="izquierda" class="altura">

			<div id="seccion">Modificar perfil</div>

			<s:form action="ModificarUsuario" method="post" validation="true">				
				<s:textfield name="nombre" label="Nombre" />
				<s:textfield name="apellido" label="Apellido" />
				<s:password name="new_pass1" label="Contraseña nueva"/>
				<s:password name="new_pass2" label="Confirmar contraseña" />
				<s:password name="password" label="Contraseña" />
				<s:submit value="Hecho" />
			</s:form>

		</div>
		
	<div id="derecha" class="altura">

			<div id="usuario">
				<s:property value="#session.usuario.getLogin()" />
			</div>

			<s:if test="%{#session.usuario.getRol() =='editor'}">
			    <div id="opcion">
					<a href="RegistrarRevisor.jsp">Registrar Revisor</a>
				</div>
				
				<div id="opcion">
					<s:form action="ConsultarArticulos">
						<s:submit value="Subir Informe" />
					</s:form>
				</div>

				<div id="opcion">
					<s:form action="ConsultarRevisores">
						<s:submit value="Asignar Revisores" />
					</s:form>
				</div>

				<div id="opcion">
					<s:form action="VerInformes">
						<s:submit value="Ver Informes" />
					</s:form>
				</div>
			</s:if>
			<s:else>
				<s:if test="%{#session.usuario.getRol() == 'revisor'}">
					<div id="opcion">
						<a href="SubirRevision.jsp">Subir Revisión</a>
					</div>

					<div id="opcion">
						<s:form action="VerRevisiones">
							<s:submit value="Ver Revisiones" />
						</s:form>
					</div>
				</s:if>
				<s:else>
					<div id="opcion">
						<a href="SubirArticulo.jsp">Subir Articulo</a>
					</div>

					<div id="opcion">
						<s:form action="VerArticulos">
							<s:submit value="Ver Articulos" />
						</s:form>
					</div>
				</s:else>
			</s:else>
			<div id="opcion">
				<s:form action="Desconectar">
					<s:submit value="Desconectar" />
				</s:form>

			</div>

		</div>

	</div>

	<div id="pie">
		<h3>Journal Technologies</h3>
	</div>

</body>
</html>