var classdominio_1_1_articulo =
[
    [ "Articulo", "classdominio_1_1_articulo.html#abe31e67dc998e8e678c55897095338c6", null ],
    [ "Articulo", "classdominio_1_1_articulo.html#a2304f947b66e7cafb7294a15e1c581cd", null ],
    [ "Articulo", "classdominio_1_1_articulo.html#a5bf90de01f09f723428450c7936c1438", null ],
    [ "getArchivo", "classdominio_1_1_articulo.html#af87b5e4e88949228c7540877ef86b7b7", null ],
    [ "getAutor", "classdominio_1_1_articulo.html#a5192461ca9b01a545755362cf9dbbc13", null ],
    [ "getEstado", "classdominio_1_1_articulo.html#aaba918ccde52370975fb96651bdc3d05", null ],
    [ "getId", "classdominio_1_1_articulo.html#a69ae4688a6c212c55449c06a5dc7e279", null ],
    [ "getInforme", "classdominio_1_1_articulo.html#a8385f50500ac64fb5315ba6a7535dcc6", null ],
    [ "getNrevisiones", "classdominio_1_1_articulo.html#aba92ec6416933269d56de64ee6b65f68", null ],
    [ "getTemas", "classdominio_1_1_articulo.html#aa45606a8325edec25686f90c55cbce63", null ],
    [ "getTitulo", "classdominio_1_1_articulo.html#a10043e5c26282dace15fdcd4581e68ba", null ],
    [ "setArchivo", "classdominio_1_1_articulo.html#a18dd51d081c5aacfb0d84045d3a0d45d", null ],
    [ "setAutor", "classdominio_1_1_articulo.html#ab2f0ad26fd8845e2d3de555828168014", null ],
    [ "setEstado", "classdominio_1_1_articulo.html#a72de1a152acf628d8564051b25affb1a", null ],
    [ "setId", "classdominio_1_1_articulo.html#a92dc26bc618fb322919f77acb9660546", null ],
    [ "setInforme", "classdominio_1_1_articulo.html#a88076bd8581095c9e895c9f94953604a", null ],
    [ "setNrevisiones", "classdominio_1_1_articulo.html#a421f54e0072420e5614d46cb7d55cf95", null ],
    [ "setTemas", "classdominio_1_1_articulo.html#ae61135f2ed765c7674cb270334d3636f", null ],
    [ "setTitulo", "classdominio_1_1_articulo.html#a0baf2e00e37785a3256a5c637cbd9cdb", null ]
];