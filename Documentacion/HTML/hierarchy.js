var hierarchy =
[
    [ "util.Archivo", "classutil_1_1_archivo.html", null ],
    [ "dominio.Articulo", "classdominio_1_1_articulo.html", null ],
    [ "persistencia.ArticuloDAO", "classpersistencia_1_1_articulo_d_a_o.html", null ],
    [ "excepciones.ArticuloErroneoException", "classexcepciones_1_1_articulo_erroneo_exception.html", null ],
    [ "excepciones.ArticuloNuevoException", "classexcepciones_1_1_articulo_nuevo_exception.html", null ],
    [ "acciones.editor.AsignarRevisor", "classacciones_1_1editor_1_1_asignar_revisor.html", null ],
    [ "persistencia.Broker", "classpersistencia_1_1_broker.html", null ],
    [ "util.cifrarPass", "classutil_1_1cifrar_pass.html", null ],
    [ "persistencia.Conexion", "classpersistencia_1_1_conexion.html", null ],
    [ "acciones.editor.ConsultarArticulos", "classacciones_1_1editor_1_1_consultar_articulos.html", null ],
    [ "acciones.revisor.ConsultarRevisiones", "classacciones_1_1revisor_1_1_consultar_revisiones.html", null ],
    [ "acciones.editor.ConsultarRevisores", "classacciones_1_1editor_1_1_consultar_revisores.html", null ],
    [ "util.Email", "classutil_1_1_email.html", null ],
    [ "acciones.autor.GenerarPDFArticuloInforme", "classacciones_1_1autor_1_1_generar_p_d_f_articulo_informe.html", null ],
    [ "acciones.autor.GestionarArticulo", "classacciones_1_1autor_1_1_gestionar_articulo.html", [
      [ "acciones.autor.GenerarPDFArticulo", "classacciones_1_1autor_1_1_generar_p_d_f_articulo.html", null ],
      [ "acciones.autor.SubirArticulo", "classacciones_1_1autor_1_1_subir_articulo.html", null ],
      [ "acciones.autor.VerArticulos", "classacciones_1_1autor_1_1_ver_articulos.html", null ]
    ] ],
    [ "acciones.editor.GestionarInforme", "classacciones_1_1editor_1_1_gestionar_informe.html", [
      [ "acciones.editor.GenerarInforme", "classacciones_1_1editor_1_1_generar_informe.html", null ],
      [ "acciones.editor.GenerarPDFInforme", "classacciones_1_1editor_1_1_generar_p_d_f_informe.html", null ],
      [ "acciones.editor.GenerarPDFInformeArticulo", "classacciones_1_1editor_1_1_generar_p_d_f_informe_articulo.html", null ],
      [ "acciones.editor.SubirInforme", "classacciones_1_1editor_1_1_subir_informe.html", null ],
      [ "acciones.editor.VerInformes", "classacciones_1_1editor_1_1_ver_informes.html", null ]
    ] ],
    [ "acciones.revisor.GestionarRevision", "classacciones_1_1revisor_1_1_gestionar_revision.html", [
      [ "acciones.revisor.AceptarRevision", "classacciones_1_1revisor_1_1_aceptar_revision.html", null ],
      [ "acciones.revisor.GenerarPDFArticuloRevision", "classacciones_1_1revisor_1_1_generar_p_d_f_articulo_revision.html", null ],
      [ "acciones.revisor.GenerarPDFRevision", "classacciones_1_1revisor_1_1_generar_p_d_f_revision.html", null ],
      [ "acciones.revisor.RechazarRevision", "classacciones_1_1revisor_1_1_rechazar_revision.html", null ],
      [ "acciones.revisor.SubirRevision", "classacciones_1_1revisor_1_1_subir_revision.html", null ],
      [ "acciones.revisor.VerRevisiones", "classacciones_1_1revisor_1_1_ver_revisiones.html", null ]
    ] ],
    [ "acciones.usuario.GestionarUsuario", "classacciones_1_1usuario_1_1_gestionar_usuario.html", [
      [ "acciones.editor.RegistrarRevisor", "classacciones_1_1editor_1_1_registrar_revisor.html", null ],
      [ "acciones.usuario.Autenticar", "classacciones_1_1usuario_1_1_autenticar.html", null ],
      [ "acciones.usuario.Desconectar", "classacciones_1_1usuario_1_1_desconectar.html", null ],
      [ "acciones.usuario.ModificarUsuario", "classacciones_1_1usuario_1_1_modificar_usuario.html", null ],
      [ "acciones.usuario.Registrar", "classacciones_1_1usuario_1_1_registrar.html", null ]
    ] ],
    [ "dominio.Informe", "classdominio_1_1_informe.html", null ],
    [ "persistencia.InformeDAO", "classpersistencia_1_1_informe_d_a_o.html", null ],
    [ "excepciones.InformeErroneoException", "classexcepciones_1_1_informe_erroneo_exception.html", null ],
    [ "excepciones.InformeNuevoException", "classexcepciones_1_1_informe_nuevo_exception.html", null ],
    [ "excepciones.NoHayConexionException", "classexcepciones_1_1_no_hay_conexion_exception.html", null ],
    [ "dominio.Revision", "classdominio_1_1_revision.html", null ],
    [ "persistencia.RevisionDAO", "classpersistencia_1_1_revision_d_a_o.html", null ],
    [ "excepciones.RevisionErroneaException", "classexcepciones_1_1_revision_erronea_exception.html", null ],
    [ "excepciones.RevisionNuevaException", "classexcepciones_1_1_revision_nueva_exception.html", null ],
    [ "dominio.Usuario", "classdominio_1_1_usuario.html", [
      [ "dominio.Autor", "classdominio_1_1_autor.html", null ],
      [ "dominio.Editor", "classdominio_1_1_editor.html", null ],
      [ "dominio.Revisor", "classdominio_1_1_revisor.html", null ]
    ] ],
    [ "persistencia.UsuarioDAO", "classpersistencia_1_1_usuario_d_a_o.html", null ],
    [ "excepciones.UsuarioErroneo", "classexcepciones_1_1_usuario_erroneo.html", null ],
    [ "excepciones.UsuarioErroneoException", "classexcepciones_1_1_usuario_erroneo_exception.html", null ],
    [ "excepciones.UsuarioNuevoException", "classexcepciones_1_1_usuario_nuevo_exception.html", null ]
];