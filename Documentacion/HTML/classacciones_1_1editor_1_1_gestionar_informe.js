var classacciones_1_1editor_1_1_gestionar_informe =
[
    [ "getArticulo", "classacciones_1_1editor_1_1_gestionar_informe.html#afa43a684c2e9455de9c8dbb037c02436", null ],
    [ "getEditor", "classacciones_1_1editor_1_1_gestionar_informe.html#ad5e59ac5fccad923e91b7aea881d0bdc", null ],
    [ "getInforme", "classacciones_1_1editor_1_1_gestionar_informe.html#afe9d4f4aa8c46bdf0411a4635f4ff240", null ],
    [ "getInformes", "classacciones_1_1editor_1_1_gestionar_informe.html#ad4ed40bbc7e07ca529eabc7e8652cad8", null ],
    [ "getTitulo", "classacciones_1_1editor_1_1_gestionar_informe.html#a1fd5691c8d035f4340a80271e9618991", null ],
    [ "setArticulo", "classacciones_1_1editor_1_1_gestionar_informe.html#a13d995b473560b7ffa511ad0ce43d79d", null ],
    [ "setEditor", "classacciones_1_1editor_1_1_gestionar_informe.html#a6c29b3a8fea25b303937172231d2866c", null ],
    [ "setInforme", "classacciones_1_1editor_1_1_gestionar_informe.html#add77da5b40bc2fd659107b025477d309", null ],
    [ "setInformes", "classacciones_1_1editor_1_1_gestionar_informe.html#a5a6dc980aea0fe1a105f4da9d0262bbd", null ],
    [ "setTitulo", "classacciones_1_1editor_1_1_gestionar_informe.html#a2ac8520ae5f11cf532d84225d83a6e24", null ],
    [ "articulo", "classacciones_1_1editor_1_1_gestionar_informe.html#aa4882d1f1b19a51aeaf3da2a41c8db95", null ],
    [ "editor", "classacciones_1_1editor_1_1_gestionar_informe.html#a54c25c435a2402001cd23fb45e7275df", null ],
    [ "informe", "classacciones_1_1editor_1_1_gestionar_informe.html#a54ab9487a109a65d259389e02c41cf36", null ],
    [ "informes", "classacciones_1_1editor_1_1_gestionar_informe.html#ac3d01eb69d57c34e0ac46eedb7eff881", null ],
    [ "titulo", "classacciones_1_1editor_1_1_gestionar_informe.html#ac70b91984eceb61e2c188818c3651fdc", null ]
];