var classacciones_1_1autor_1_1_gestionar_articulo =
[
    [ "anadirArticulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#a605bde5f6fdc730bd66b6e864c5c9516", null ],
    [ "getArticulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#aa8120077d9d75c83224f97486092ed85", null ],
    [ "getArticulos", "classacciones_1_1autor_1_1_gestionar_articulo.html#ae3605fb9d15703a8eda9c2ec2daf4a7f", null ],
    [ "getAutor", "classacciones_1_1autor_1_1_gestionar_articulo.html#a47743ea523e8ee13538bbf611ce1bc63", null ],
    [ "getTemas", "classacciones_1_1autor_1_1_gestionar_articulo.html#a499ab23235da144a8653f83a8d97be72", null ],
    [ "getTitulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#ac70dd70263138291f919c1fe26b8a126", null ],
    [ "setArticulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#a9c7f1579340db077034d06e0c4d87d96", null ],
    [ "setArticulos", "classacciones_1_1autor_1_1_gestionar_articulo.html#aff4084925236d1faa42c7fbbd348dcee", null ],
    [ "setAutor", "classacciones_1_1autor_1_1_gestionar_articulo.html#ad70428637142719ad5ccfa12a5703370", null ],
    [ "setTemas", "classacciones_1_1autor_1_1_gestionar_articulo.html#ac5a77d3ea408292c2609982f5b5eb2ed", null ],
    [ "setTitulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#a570703798d77eed021a977fcd8567fc1", null ],
    [ "articulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#a3a1666b3e70ebed8909394a6210c63be", null ],
    [ "articulos", "classacciones_1_1autor_1_1_gestionar_articulo.html#ac5834c361c516a716cc47a5920c6ea6f", null ],
    [ "autor", "classacciones_1_1autor_1_1_gestionar_articulo.html#aa322bb062a783d0e945a5598d6d5a9dd", null ],
    [ "temas", "classacciones_1_1autor_1_1_gestionar_articulo.html#aa7c161cec4a74a7724c216acca7d0003", null ],
    [ "titulo", "classacciones_1_1autor_1_1_gestionar_articulo.html#a341744f9f41e099b6659d830e1ab3b8d", null ]
];