var classacciones_1_1revisor_1_1_gestionar_revision =
[
    [ "getBaremo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a64f8ec46b3fc423f92e4a9074d87f8fb", null ],
    [ "getEstado", "classacciones_1_1revisor_1_1_gestionar_revision.html#af7d6a0c06bb8b6718d8850b69b754ad1", null ],
    [ "getNrevisiones", "classacciones_1_1revisor_1_1_gestionar_revision.html#a7c010ff40c2f28963fea7c9f84b387ca", null ],
    [ "getRevision", "classacciones_1_1revisor_1_1_gestionar_revision.html#a215c4ba8c2bc9b157b4476fdb67baad7", null ],
    [ "getRevisionesAceptadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#ab1efef90340fafae8f3218fe8829cad1", null ],
    [ "getRevisionesAsignadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#a3b5dd18ae8502ece7d37f44be3b4d3e9", null ],
    [ "getRevisionesTerminadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#a30d4e4b067bb4c42d5d1a8f6a19ded28", null ],
    [ "getRevisor", "classacciones_1_1revisor_1_1_gestionar_revision.html#a063a3ecd8b64c0a1e6c6d54afc0b4ff0", null ],
    [ "getTitulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a93973687b6ae9668dea3c0f598c3eea7", null ],
    [ "getTituloArticulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a11da186a1f1235fcb5f2649864fa3777", null ],
    [ "setBaremo", "classacciones_1_1revisor_1_1_gestionar_revision.html#afd68ee67706a411e79d21f8c86cc253f", null ],
    [ "setEstado", "classacciones_1_1revisor_1_1_gestionar_revision.html#abc25f926277de6562e3b9bdc08122b9e", null ],
    [ "setNrevisiones", "classacciones_1_1revisor_1_1_gestionar_revision.html#ac16745d7ce4a1bc06c0b1f070d99ed7d", null ],
    [ "setRevision", "classacciones_1_1revisor_1_1_gestionar_revision.html#a9b7054b1e8bb9030cb5e6f51fc98cf85", null ],
    [ "setRevisionesAceptadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#a8715fe733ec33e32a9a8465b21c1c9bc", null ],
    [ "setRevisionesAsignadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#a6a0316ae4be655029a630924a5898158", null ],
    [ "setRevisionesTerminadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#aaf005347a46e47b9b03a73d4173a7a04", null ],
    [ "setRevisor", "classacciones_1_1revisor_1_1_gestionar_revision.html#adf98a9e179a2b1192de84789d1f649eb", null ],
    [ "setTitulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a41f71858b7d85bce685aa24bfc731666", null ],
    [ "setTituloArticulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a4ccebec854b353cc395604093f8ba931", null ],
    [ "baremo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a4492e25e6daf71ca97d92280cbceb686", null ],
    [ "estado", "classacciones_1_1revisor_1_1_gestionar_revision.html#a9e30a1ebd015d936d2631dbd09e256d4", null ],
    [ "nrevisiones", "classacciones_1_1revisor_1_1_gestionar_revision.html#a4ca5881dde12ad56372b6ce00e4cc976", null ],
    [ "revision", "classacciones_1_1revisor_1_1_gestionar_revision.html#a1f042b7546c89ce0f883ec2d713322ae", null ],
    [ "revisionesAceptadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#a6bab4b2ae23d8c9180b997b73fff1ba2", null ],
    [ "revisionesAsignadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#ad95427166691eced9330dcf2e2764989", null ],
    [ "revisionesTerminadas", "classacciones_1_1revisor_1_1_gestionar_revision.html#ae08eb576040d92d91d6ac49b639f1e2c", null ],
    [ "revisor", "classacciones_1_1revisor_1_1_gestionar_revision.html#a4ee8cc5c789dedee84a06304fd4a3755", null ],
    [ "titulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#a7ca7e12633beb12682bcba1a6ada4c2c", null ],
    [ "tituloArticulo", "classacciones_1_1revisor_1_1_gestionar_revision.html#aaec264dcf40dc1fdab19feebdff36814", null ]
];