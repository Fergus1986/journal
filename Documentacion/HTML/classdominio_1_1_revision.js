var classdominio_1_1_revision =
[
    [ "Revision", "classdominio_1_1_revision.html#a3c635800ef438efb14f1f69a21f2b0cd", null ],
    [ "Revision", "classdominio_1_1_revision.html#a6a23cc4fc093a93bde619166e16d9de9", null ],
    [ "Revision", "classdominio_1_1_revision.html#a8f4817a9ea6bd14264c430c42bcabf42", null ],
    [ "Revision", "classdominio_1_1_revision.html#ada3595567bcc061cdfbe383da062b403", null ],
    [ "getArchivo", "classdominio_1_1_revision.html#a6054eb044102d0411f9822449e80c5ae", null ],
    [ "getArticulo", "classdominio_1_1_revision.html#a105a6475b31f0303673efde622f85fc9", null ],
    [ "getBaremo", "classdominio_1_1_revision.html#aee55229e754487cb95dd35db30792e5c", null ],
    [ "getEstado", "classdominio_1_1_revision.html#a432e7ae7f2fb5cdccad66935aea3c087", null ],
    [ "getRevisor", "classdominio_1_1_revision.html#ad773eeb8dff487ecea68d0bed79968c0", null ],
    [ "getTemasArticulo", "classdominio_1_1_revision.html#a52d3074ffaab60463932764a97c01a34", null ],
    [ "getTituloArticulo", "classdominio_1_1_revision.html#abacbf9e9b6e97b6e57719b6cb619c1d2", null ],
    [ "rechazar", "classdominio_1_1_revision.html#a0eff6f7c1ae5da183cb82c524b741411", null ],
    [ "setArchivo", "classdominio_1_1_revision.html#a2799729003c321a9790388f8574b6612", null ],
    [ "setArticulo", "classdominio_1_1_revision.html#ab743942400626dcfab3b5b3072dbbe81", null ],
    [ "setBaremo", "classdominio_1_1_revision.html#a6e9ff3ee17bca8ca11c67d303906be1f", null ],
    [ "setEstado", "classdominio_1_1_revision.html#a0ab4fd08c7f13f58045f9c6e8fda0281", null ],
    [ "setRevisor", "classdominio_1_1_revision.html#aaa2b1277bd0860d131fb41a17cea4550", null ]
];