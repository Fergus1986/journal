var searchData=
[
  ['generarinforme',['GenerarInforme',['../classacciones_1_1editor_1_1_generar_informe.html',1,'acciones::editor']]],
  ['generarpdfarticulo',['GenerarPDFArticulo',['../classacciones_1_1autor_1_1_generar_p_d_f_articulo.html',1,'acciones::autor']]],
  ['generarpdfarticuloinforme',['GenerarPDFArticuloInforme',['../classacciones_1_1autor_1_1_generar_p_d_f_articulo_informe.html',1,'acciones::autor']]],
  ['generarpdfarticulorevision',['GenerarPDFArticuloRevision',['../classacciones_1_1revisor_1_1_generar_p_d_f_articulo_revision.html',1,'acciones::revisor']]],
  ['generarpdfinforme',['GenerarPDFInforme',['../classacciones_1_1editor_1_1_generar_p_d_f_informe.html',1,'acciones::editor']]],
  ['generarpdfinformearticulo',['GenerarPDFInformeArticulo',['../classacciones_1_1editor_1_1_generar_p_d_f_informe_articulo.html',1,'acciones::editor']]],
  ['generarpdfrevision',['GenerarPDFRevision',['../classacciones_1_1revisor_1_1_generar_p_d_f_revision.html',1,'acciones::revisor']]],
  ['gestionararticulo',['GestionarArticulo',['../classacciones_1_1autor_1_1_gestionar_articulo.html',1,'acciones::autor']]],
  ['gestionarinforme',['GestionarInforme',['../classacciones_1_1editor_1_1_gestionar_informe.html',1,'acciones::editor']]],
  ['gestionarrevision',['GestionarRevision',['../classacciones_1_1revisor_1_1_gestionar_revision.html',1,'acciones::revisor']]],
  ['gestionarusuario',['GestionarUsuario',['../classacciones_1_1usuario_1_1_gestionar_usuario.html',1,'acciones::usuario']]]
];
