var classacciones_1_1usuario_1_1_modificar_usuario =
[
    [ "execute", "classacciones_1_1usuario_1_1_modificar_usuario.html#a5ef908b7434a738e3a19b76b969defee", null ],
    [ "getApellido", "classacciones_1_1usuario_1_1_modificar_usuario.html#a60c26a826c13e97352e2bd7248aa0942", null ],
    [ "getNew_pass1", "classacciones_1_1usuario_1_1_modificar_usuario.html#a0fb36c9b8dea15fb619c5d30bd66e469", null ],
    [ "getNew_pass2", "classacciones_1_1usuario_1_1_modificar_usuario.html#a83a170cbb1f06f615f55430c447227d1", null ],
    [ "getNombre", "classacciones_1_1usuario_1_1_modificar_usuario.html#aa7b25ee247dbfe410a2d7ed9eaa0286d", null ],
    [ "getSesion", "classacciones_1_1usuario_1_1_modificar_usuario.html#a8c4e8f1c3aed8c45769ca833eb6dccf0", null ],
    [ "setApellido", "classacciones_1_1usuario_1_1_modificar_usuario.html#a68a25da59c56b6d357855371ec953647", null ],
    [ "setNew_pass1", "classacciones_1_1usuario_1_1_modificar_usuario.html#aa2fdccd3a76e0ebb86affa7ff9af9a05", null ],
    [ "setNew_pass2", "classacciones_1_1usuario_1_1_modificar_usuario.html#a61f772b257f60ea3e0f2cea610c66c7b", null ],
    [ "setNombre", "classacciones_1_1usuario_1_1_modificar_usuario.html#a597692b46b6d6e903f289bfd608a7079", null ],
    [ "setSesion", "classacciones_1_1usuario_1_1_modificar_usuario.html#a4aab445a96127d24373c0d6169cfd787", null ],
    [ "setSession", "classacciones_1_1usuario_1_1_modificar_usuario.html#a52290716d148ccfc7c2bbdb5f348c5ae", null ],
    [ "validate", "classacciones_1_1usuario_1_1_modificar_usuario.html#aef0e562851ac1b1114c76cb630e199de", null ]
];