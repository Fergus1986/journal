var classutil_1_1_email =
[
    [ "Email", "classutil_1_1_email.html#a0d7850dc767e4c7d2218a459b31b1af1", null ],
    [ "getConcepto", "classutil_1_1_email.html#a644b5cf6c11e644864bafd36451f9d86", null ],
    [ "getContenido", "classutil_1_1_email.html#a82d1065bd71096b0265c49c0d5659194", null ],
    [ "getDestinatario", "classutil_1_1_email.html#a3ad34ef8bb1b7479ba89e5174a70a077", null ],
    [ "getHost", "classutil_1_1_email.html#a797201906813ffd59c8ce839efa4d088", null ],
    [ "getMensaje", "classutil_1_1_email.html#ab343ddb974eaf9ad05728c272edad1a1", null ],
    [ "getPassword", "classutil_1_1_email.html#aee72d3275515bc2e54e08e417673f0a5", null ],
    [ "getPropiedades", "classutil_1_1_email.html#acab7f1c01dbf9a46d4037c7b0b60cb20", null ],
    [ "getSesion", "classutil_1_1_email.html#a163345048595dfd6f8549c96c2de44ad", null ],
    [ "getUsuario", "classutil_1_1_email.html#ae0614ad619374fabdcb876cde0193b53", null ],
    [ "setConcepto", "classutil_1_1_email.html#a3ee23688729ae6a5d198ffa3759ce848", null ],
    [ "setContenido", "classutil_1_1_email.html#a3fd7fa82132ee0c7265553a44765c6e9", null ],
    [ "setDestinatario", "classutil_1_1_email.html#aa7516e239f320e61a83ebc28776aa40e", null ],
    [ "setHost", "classutil_1_1_email.html#ac51e9280a513ae111d7530a3407c516e", null ],
    [ "setMensaje", "classutil_1_1_email.html#aa61019ded770110c454b68c199690ac8", null ],
    [ "setPassword", "classutil_1_1_email.html#a4378aaba854e897100e38a37558fc7aa", null ],
    [ "setPropiedades", "classutil_1_1_email.html#a034dc12a58b8f0cde3b0a8dae3ea965a", null ],
    [ "setSesion", "classutil_1_1_email.html#a0a7d8409bea5705716648a82fb907eec", null ],
    [ "setUsuario", "classutil_1_1_email.html#ac2c2608ec882fdffbfe393b6d7a1a6b3", null ]
];