var annotated =
[
    [ "acciones", null, [
      [ "autor", null, [
        [ "GenerarPDFArticulo", "classacciones_1_1autor_1_1_generar_p_d_f_articulo.html", "classacciones_1_1autor_1_1_generar_p_d_f_articulo" ],
        [ "GenerarPDFArticuloInforme", "classacciones_1_1autor_1_1_generar_p_d_f_articulo_informe.html", "classacciones_1_1autor_1_1_generar_p_d_f_articulo_informe" ],
        [ "GestionarArticulo", "classacciones_1_1autor_1_1_gestionar_articulo.html", "classacciones_1_1autor_1_1_gestionar_articulo" ],
        [ "SubirArticulo", "classacciones_1_1autor_1_1_subir_articulo.html", "classacciones_1_1autor_1_1_subir_articulo" ],
        [ "VerArticulos", "classacciones_1_1autor_1_1_ver_articulos.html", "classacciones_1_1autor_1_1_ver_articulos" ]
      ] ],
      [ "editor", null, [
        [ "AsignarRevisor", "classacciones_1_1editor_1_1_asignar_revisor.html", "classacciones_1_1editor_1_1_asignar_revisor" ],
        [ "ConsultarArticulos", "classacciones_1_1editor_1_1_consultar_articulos.html", "classacciones_1_1editor_1_1_consultar_articulos" ],
        [ "ConsultarRevisores", "classacciones_1_1editor_1_1_consultar_revisores.html", "classacciones_1_1editor_1_1_consultar_revisores" ],
        [ "GenerarInforme", "classacciones_1_1editor_1_1_generar_informe.html", "classacciones_1_1editor_1_1_generar_informe" ],
        [ "GenerarPDFInforme", "classacciones_1_1editor_1_1_generar_p_d_f_informe.html", "classacciones_1_1editor_1_1_generar_p_d_f_informe" ],
        [ "GenerarPDFInformeArticulo", "classacciones_1_1editor_1_1_generar_p_d_f_informe_articulo.html", "classacciones_1_1editor_1_1_generar_p_d_f_informe_articulo" ],
        [ "GestionarInforme", "classacciones_1_1editor_1_1_gestionar_informe.html", "classacciones_1_1editor_1_1_gestionar_informe" ],
        [ "RegistrarRevisor", "classacciones_1_1editor_1_1_registrar_revisor.html", "classacciones_1_1editor_1_1_registrar_revisor" ],
        [ "SubirInforme", "classacciones_1_1editor_1_1_subir_informe.html", "classacciones_1_1editor_1_1_subir_informe" ],
        [ "VerInformes", "classacciones_1_1editor_1_1_ver_informes.html", "classacciones_1_1editor_1_1_ver_informes" ]
      ] ],
      [ "revisor", null, [
        [ "AceptarRevision", "classacciones_1_1revisor_1_1_aceptar_revision.html", "classacciones_1_1revisor_1_1_aceptar_revision" ],
        [ "ConsultarRevisiones", "classacciones_1_1revisor_1_1_consultar_revisiones.html", "classacciones_1_1revisor_1_1_consultar_revisiones" ],
        [ "GenerarPDFArticuloRevision", "classacciones_1_1revisor_1_1_generar_p_d_f_articulo_revision.html", "classacciones_1_1revisor_1_1_generar_p_d_f_articulo_revision" ],
        [ "GenerarPDFRevision", "classacciones_1_1revisor_1_1_generar_p_d_f_revision.html", "classacciones_1_1revisor_1_1_generar_p_d_f_revision" ],
        [ "GestionarRevision", "classacciones_1_1revisor_1_1_gestionar_revision.html", "classacciones_1_1revisor_1_1_gestionar_revision" ],
        [ "RechazarRevision", "classacciones_1_1revisor_1_1_rechazar_revision.html", "classacciones_1_1revisor_1_1_rechazar_revision" ],
        [ "SubirRevision", "classacciones_1_1revisor_1_1_subir_revision.html", "classacciones_1_1revisor_1_1_subir_revision" ],
        [ "VerRevisiones", "classacciones_1_1revisor_1_1_ver_revisiones.html", "classacciones_1_1revisor_1_1_ver_revisiones" ]
      ] ],
      [ "usuario", null, [
        [ "Autenticar", "classacciones_1_1usuario_1_1_autenticar.html", "classacciones_1_1usuario_1_1_autenticar" ],
        [ "Desconectar", "classacciones_1_1usuario_1_1_desconectar.html", "classacciones_1_1usuario_1_1_desconectar" ],
        [ "GestionarUsuario", "classacciones_1_1usuario_1_1_gestionar_usuario.html", "classacciones_1_1usuario_1_1_gestionar_usuario" ],
        [ "ModificarUsuario", "classacciones_1_1usuario_1_1_modificar_usuario.html", "classacciones_1_1usuario_1_1_modificar_usuario" ],
        [ "Registrar", "classacciones_1_1usuario_1_1_registrar.html", "classacciones_1_1usuario_1_1_registrar" ]
      ] ]
    ] ],
    [ "dominio", null, [
      [ "Articulo", "classdominio_1_1_articulo.html", "classdominio_1_1_articulo" ],
      [ "Autor", "classdominio_1_1_autor.html", "classdominio_1_1_autor" ],
      [ "Editor", "classdominio_1_1_editor.html", "classdominio_1_1_editor" ],
      [ "Informe", "classdominio_1_1_informe.html", "classdominio_1_1_informe" ],
      [ "Revision", "classdominio_1_1_revision.html", "classdominio_1_1_revision" ],
      [ "Revisor", "classdominio_1_1_revisor.html", "classdominio_1_1_revisor" ],
      [ "Usuario", "classdominio_1_1_usuario.html", "classdominio_1_1_usuario" ]
    ] ],
    [ "excepciones", null, [
      [ "ArticuloErroneoException", "classexcepciones_1_1_articulo_erroneo_exception.html", "classexcepciones_1_1_articulo_erroneo_exception" ],
      [ "ArticuloNuevoException", "classexcepciones_1_1_articulo_nuevo_exception.html", "classexcepciones_1_1_articulo_nuevo_exception" ],
      [ "InformeErroneoException", "classexcepciones_1_1_informe_erroneo_exception.html", "classexcepciones_1_1_informe_erroneo_exception" ],
      [ "InformeNuevoException", "classexcepciones_1_1_informe_nuevo_exception.html", "classexcepciones_1_1_informe_nuevo_exception" ],
      [ "NoHayConexionException", "classexcepciones_1_1_no_hay_conexion_exception.html", "classexcepciones_1_1_no_hay_conexion_exception" ],
      [ "RevisionErroneaException", "classexcepciones_1_1_revision_erronea_exception.html", "classexcepciones_1_1_revision_erronea_exception" ],
      [ "RevisionNuevaException", "classexcepciones_1_1_revision_nueva_exception.html", "classexcepciones_1_1_revision_nueva_exception" ],
      [ "UsuarioErroneo", "classexcepciones_1_1_usuario_erroneo.html", "classexcepciones_1_1_usuario_erroneo" ],
      [ "UsuarioErroneoException", "classexcepciones_1_1_usuario_erroneo_exception.html", "classexcepciones_1_1_usuario_erroneo_exception" ],
      [ "UsuarioNuevoException", "classexcepciones_1_1_usuario_nuevo_exception.html", "classexcepciones_1_1_usuario_nuevo_exception" ]
    ] ],
    [ "persistencia", null, [
      [ "ArticuloDAO", "classpersistencia_1_1_articulo_d_a_o.html", "classpersistencia_1_1_articulo_d_a_o" ],
      [ "Broker", "classpersistencia_1_1_broker.html", "classpersistencia_1_1_broker" ],
      [ "Conexion", "classpersistencia_1_1_conexion.html", "classpersistencia_1_1_conexion" ],
      [ "InformeDAO", "classpersistencia_1_1_informe_d_a_o.html", "classpersistencia_1_1_informe_d_a_o" ],
      [ "RevisionDAO", "classpersistencia_1_1_revision_d_a_o.html", "classpersistencia_1_1_revision_d_a_o" ],
      [ "UsuarioDAO", "classpersistencia_1_1_usuario_d_a_o.html", "classpersistencia_1_1_usuario_d_a_o" ]
    ] ],
    [ "util", null, [
      [ "Archivo", "classutil_1_1_archivo.html", "classutil_1_1_archivo" ],
      [ "cifrarPass", "classutil_1_1cifrar_pass.html", "classutil_1_1cifrar_pass" ],
      [ "Email", "classutil_1_1_email.html", "classutil_1_1_email" ]
    ] ]
];