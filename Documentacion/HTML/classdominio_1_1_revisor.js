var classdominio_1_1_revisor =
[
    [ "Revisor", "classdominio_1_1_revisor.html#a19a616d7c7fa5f415c8c6d1996822eca", null ],
    [ "consultarRevision", "classdominio_1_1_revisor.html#a2a356c35845c675f7d8a799ef239f7bb", null ],
    [ "consultarRevisionesAceptadas", "classdominio_1_1_revisor.html#a8beb7aa8426a78b66b338dea91e2fb54", null ],
    [ "consultarRevisionesAsignadas", "classdominio_1_1_revisor.html#a7523eeb61e893c3424c08dc323b7b24d", null ],
    [ "consultarRevisionesTerminadas", "classdominio_1_1_revisor.html#a10e8d64d477d77bde5a9620c6cd8a3d7", null ],
    [ "getNrevisiones", "classdominio_1_1_revisor.html#a6e35e41efab8acf7b3effcf75a71797c", null ],
    [ "getRevision", "classdominio_1_1_revisor.html#a31cddc57ef6d37472a8872731aa70de0", null ],
    [ "getRevisionesAceptadas", "classdominio_1_1_revisor.html#a58d36b8662eb1f513abf2a12f8cf0f4b", null ],
    [ "getRevisionesAsignadas", "classdominio_1_1_revisor.html#a8f8372560dc4c1d63888d95f7f0c7ad6", null ],
    [ "getRevisionesTerminadas", "classdominio_1_1_revisor.html#ab50b45e7fa57ab6c445fdc03e5205bb3", null ],
    [ "setNrevisiones", "classdominio_1_1_revisor.html#aad10daa875ba143f2f987c7c520b9faf", null ],
    [ "setRevision", "classdominio_1_1_revisor.html#a06da8e54cccc3c85659d8b988dbeda9c", null ],
    [ "setRevisionesAceptadas", "classdominio_1_1_revisor.html#a0b2c7185be55dcd231868af50fa56489", null ],
    [ "setRevisionesAsignadas", "classdominio_1_1_revisor.html#ad3b90a349df3af7885ea1f57d88ded5a", null ],
    [ "setRevisionesTerminadas", "classdominio_1_1_revisor.html#a1c25bc2c94f57302d667062b033d187c", null ],
    [ "nrevisiones", "classdominio_1_1_revisor.html#afcfa24c6605eb1712621803ea46310e5", null ],
    [ "revisionesAceptadas", "classdominio_1_1_revisor.html#a2337f7af76a9ffb1b67871392456a1ec", null ],
    [ "revisionesAsignadas", "classdominio_1_1_revisor.html#a06a2125cad15d5cbf1bfb7228f3da057", null ],
    [ "revisionesTerminadas", "classdominio_1_1_revisor.html#a167eea5824fa48d7098e809f4438232d", null ]
];