var classdominio_1_1_usuario =
[
    [ "Usuario", "classdominio_1_1_usuario.html#a0ee885166e41d12b40e06367116ec9eb", null ],
    [ "Usuario", "classdominio_1_1_usuario.html#a409ac4b5c465ea7ed7ca5304fcce884c", null ],
    [ "Usuario", "classdominio_1_1_usuario.html#a6a7bf94e8426e5351ace626b08f7424d", null ],
    [ "getApellido", "classdominio_1_1_usuario.html#a7bd81b426b379940a25fc09397bf1112", null ],
    [ "getEditor", "classdominio_1_1_usuario.html#af68f20e6c2c991558cbb1f2295b0d3b6", null ],
    [ "getId", "classdominio_1_1_usuario.html#ae721131b7dda6f1f7d386870d7b4d058", null ],
    [ "getLogin", "classdominio_1_1_usuario.html#a084007818cfb1a99b37517a052a8361f", null ],
    [ "getNombre", "classdominio_1_1_usuario.html#a0493ac995025a04318781d7b8f01f852", null ],
    [ "getNrevisiones", "classdominio_1_1_usuario.html#ab07c04d3d16249e5022d252834fcaeb4", null ],
    [ "getPass", "classdominio_1_1_usuario.html#aa1a092dad50e852ca048cd661a51893a", null ],
    [ "getRol", "classdominio_1_1_usuario.html#ac7e3875b2c22935c36bc9d563e4146be", null ],
    [ "getSin_cifrar", "classdominio_1_1_usuario.html#ab3ccf7b7888109b0a05cd5aaafff3f60", null ],
    [ "modificar", "classdominio_1_1_usuario.html#a25ac1b59107ee83b5df1a0b06c90eab1", null ],
    [ "setApellido", "classdominio_1_1_usuario.html#a30be801f4894b93596397c868cd0f014", null ],
    [ "setId", "classdominio_1_1_usuario.html#a9e3c8f10fb30e07fca0fe5689efb4da7", null ],
    [ "setLogin", "classdominio_1_1_usuario.html#ab4bd2d36716f8f18b975bcba42663c87", null ],
    [ "setNombre", "classdominio_1_1_usuario.html#a8c16b932dc3ca0eb0c212c6b5722bab7", null ],
    [ "setNrevisiones", "classdominio_1_1_usuario.html#aa1f17741cb495292313456c88271551a", null ],
    [ "setPass", "classdominio_1_1_usuario.html#ade6ad390c5a58b400d24dcf43c698ab6", null ],
    [ "setRol", "classdominio_1_1_usuario.html#a4a63c1b9366a8d9cefed8e7fdf1eefc2", null ],
    [ "setSin_cifrar", "classdominio_1_1_usuario.html#ad3dc54db59f899f022a7e115e348d15b", null ]
];