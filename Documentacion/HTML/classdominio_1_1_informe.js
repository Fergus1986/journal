var classdominio_1_1_informe =
[
    [ "Informe", "classdominio_1_1_informe.html#a931f2a4b2d016b6e8850804b55880d75", null ],
    [ "Informe", "classdominio_1_1_informe.html#a8fb4721a5c0ca01b70ab0af9ba317b44", null ],
    [ "Informe", "classdominio_1_1_informe.html#a4b35585e0a4cd82827fdb03f35d6010b", null ],
    [ "getArchivo", "classdominio_1_1_informe.html#a45ff60b532d7aa053b779a22a28b53c4", null ],
    [ "getArticulo", "classdominio_1_1_informe.html#a3347b162945bbd4331e3cd13c9583e25", null ],
    [ "getAutor", "classdominio_1_1_informe.html#a9d8b3366c627bdf40840f6636d459824", null ],
    [ "getEditor", "classdominio_1_1_informe.html#a1b5409ece7c5936c063d53f26740ee35", null ],
    [ "getInforme", "classdominio_1_1_informe.html#a422117f846815146bd0ec9dcabd13e29", null ],
    [ "getTitulo", "classdominio_1_1_informe.html#ae2f499a481a3d79220a84f7aec3b0e73", null ],
    [ "setArchivo", "classdominio_1_1_informe.html#acc9e465f035fbb249dab838b112bee2c", null ],
    [ "setArticulo", "classdominio_1_1_informe.html#a137a203cda149f3a5444b0411813f7ad", null ],
    [ "setAutor", "classdominio_1_1_informe.html#a70835accebc09a391cdcf23c139d897b", null ],
    [ "setEditor", "classdominio_1_1_informe.html#a2a38a1e34ac94626e9c9e7cccf09aec8", null ],
    [ "setInforme", "classdominio_1_1_informe.html#ae34b22b05024a77d7a797f3d6c94ae68", null ],
    [ "setTitulo", "classdominio_1_1_informe.html#a1e59cab9fb6b9d4b92b67ac84d30d503", null ]
];