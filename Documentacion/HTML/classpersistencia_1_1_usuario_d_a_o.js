var classpersistencia_1_1_usuario_d_a_o =
[
    [ "actualizarUsuario", "classpersistencia_1_1_usuario_d_a_o.html#a0411bf4b4af88b779907cdba5b4ca88a", null ],
    [ "getEditor", "classpersistencia_1_1_usuario_d_a_o.html#af04df597420a117e8387c36379592b83", null ],
    [ "getUsuario", "classpersistencia_1_1_usuario_d_a_o.html#ade56c363872f5577d6e60ca0927244e9", null ],
    [ "getUsuarios", "classpersistencia_1_1_usuario_d_a_o.html#a06108afc7fd5a4800fb2242491d25292", null ],
    [ "getUsuariosRevisores", "classpersistencia_1_1_usuario_d_a_o.html#a972910b1f34497f6a993fbb6a24bce65", null ],
    [ "insertarNuevoUsuario", "classpersistencia_1_1_usuario_d_a_o.html#ad41348b139f264e76ba13288e633eeff", null ],
    [ "modificarRevisor", "classpersistencia_1_1_usuario_d_a_o.html#ab83d006c6a41086c269641760ac3bc5e", null ],
    [ "recuperarPass", "classpersistencia_1_1_usuario_d_a_o.html#a7eff5a74d085093b0fba023c7869b32f", null ],
    [ "revisionesRevisor", "classpersistencia_1_1_usuario_d_a_o.html#a8527a2fdb136dabcce27fb545375b166", null ],
    [ "revisionTerminada", "classpersistencia_1_1_usuario_d_a_o.html#ae2ffd30ba587f2fcf95af6c8a55130a1", null ]
];