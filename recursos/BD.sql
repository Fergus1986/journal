CREATE DATABASE IF NOT EXISTS journal;
USE journal;

DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
	login VARCHAR(25) UNIQUE,
	pass VARCHAR(32) NOT NULL,
	rol VARCHAR(25) NOT NULL,
	nombre VARCHAR(25),
	apellido VARCHAR(25),
	nrevisiones SMALLINT,
	PRIMARY KEY (login)
);

DROP TABLE IF EXISTS articulo;

CREATE TABLE articulo (
	titulo VARCHAR(25) NOT NULL,
	autor VARCHAR(25) NOT NULL,
	temas VARCHAR(128) NOT NULL,
	archivo MEDIUMBLOB NOT NULL,
	estado VARCHAR(25) NOT NULL,
	nrevisiones SMALLINT,
	PRIMARY KEY(titulo, autor)
);

DROP TABLE IF EXISTS informe;
CREATE TABLE informe (
	titulo VARCHAR(25) NOT NULL,
	editor VARCHAR(25) NOT NULL,
	archivo MEDIUMBLOB NOT NULL,
	articulo VARCHAR(25) NOT NULL,
	PRIMARY KEY(titulo, editor)
);

DROP TABLE IF EXISTS revision;
CREATE TABLE revision (
	articulo VARCHAR(25) NOT NULL,
	revisor VARCHAR(25) NOT NULL,
	estado VARCHAR(25) NOT NULL,
	baremo SMALLINT,
	archivo MEDIUMBLOB,	
	PRIMARY KEY(revisor, articulo)
);

INSERT INTO usuario (login, pass, rol, nombre, apellido, nrevisiones) VALUES('fergus1986@gmail.com', '4ebb10ab6aeb99b02b0b9040955c62d8', 'autor', 'fergus', 'perez',0);
INSERT INTO usuario (login, pass, rol, nombre, apellido, nrevisiones) VALUES('dave.alcarin@gmail.com', '172522ec1028ab781d9dfd17eaca4427', 'revisor', 'david', 'garcia',9);
INSERT INTO usuario (login, pass, rol, nombre, apellido, nrevisiones) VALUES('maryna.mamaliga@gmail.com', '839c88c09a655dcb39c36ddc26c8b422', 'editor', 'maryna', 'mamaliga',0);

INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo01', 'fergus1986@gmail.com', 'tema01', '03203204204204032', 'informe',2);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo02', 'fergus1986@gmail.com', 'tema02', '03203204204204032', 'revision',4);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo03', 'fergus1986@gmail.com', 'tema03', '03203204204204032', 'informe',0);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo04', 'fergus1986@gmail.com', 'tema04', '03203204204204032', 'informe',0);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo05', 'fergus1986@gmail.com', 'tema02', '03203204204204032', 'revision',5);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo06', 'fergus1986@gmail.com', 'tema03', '03203204204204032', 'informe',0);
INSERT INTO articulo (titulo, autor, temas, archivo, estado, nrevisiones) VALUES ('articulo07', 'fergus1986@gmail.com', 'tema04', '03203204204204032', 'informe',0);

INSERT INTO revision (articulo, revisor, estado, baremo, archivo) VALUES ('articulo01', 'dave.alcarin@gmail.com', 'terminada', 4, 03203204204204032);
INSERT INTO revision (articulo, revisor, estado, baremo, archivo) VALUES ('articulo02', 'dave.alcarin@gmail.com', 'terminada', 3, 03203204204204032);
INSERT INTO revision (articulo, revisor, estado, baremo, archivo) VALUES ('articulo03', 'dave.alcarin@gmail.com', 'asignada', NULL, NULL);
INSERT INTO revision (articulo, revisor, estado, baremo, archivo) VALUES ('articulo04', 'dave.alcarin@gmail.com', 'asignada', NULL, NULL);
INSERT INTO revision (articulo, revisor, estado, baremo, archivo) VALUES ('articulo01', 'paco', 'terminada', 4, 03203204204204032);

INSERT INTO informe (titulo, editor, archivo,articulo) VALUES ('informe01', 'maryna.mamaliga@gmail.com',  'informe de prueba','articulo01');
INSERT INTO informe (titulo, editor, archivo,articulo) VALUES ('informe03', 'maryna.mamaliga@gmail.com',  '03203204204204032','articulo03');
INSERT INTO informe (titulo, editor, archivo,articulo) VALUES ('informe04', 'maryna.mamaliga@gmail.com',  '03203204204204032','articulo04');
INSERT INTO informe (titulo, editor, archivo,articulo) VALUES ('informe06', 'maryna.mamaliga@gmail.com',  '03203204204204032','articulo06');
INSERT INTO informe (titulo, editor, archivo,articulo) VALUES ('informe07', 'maryna.mamaliga@gmail.com',  '03203204204204032','articulo07');
